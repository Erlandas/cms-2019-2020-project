package db;

/**
 * @author  Erlandas Bacauskas
 *<br><b>Description</b> : <br>
 *      This class holds few shared methods between tables in database
 *      Also as connection methods
 */

import gui.shared.SharedTableModel;

import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

public class Datasource {

    //Data required for connection to database
    private static final String DB_NAME = "cms";
    private static final String DB_URL = "jdbc:mysql://localhost/";
    private static final String DB_PASSWORD = "";
    private static final String DB_USER = "root";

    //Connection variables
    public static Connection conn;

    /**
     *
     * @return type boolean
     * <br><b>Description</b> : <br> establishes connection with database
     */
    //Method to establish connection with database
    public static boolean establishConnection() {
        try {
            conn = DriverManager.getConnection(DB_URL + DB_NAME,DB_USER,DB_PASSWORD);
            return true;
        } catch(SQLException e) {
            return false;
        }
    }

    /**<br><b>Description</b> : <br>
     * Closes connection to database
     */
    //Method to close connection with database
    public static void closeConnection(){
        try {
           if(conn != null) {
               conn.close();
           }
        } catch(SQLException e) {
        }
    }


    /**
     *
     * @param colNames  -   type Vector<String>
     * @param sql       -   type String
     * @return JTable
     * <br><b>Description</b> : <br> returns JTable with data retrieved using SQL syntax for specific table
     */
    //Creates JTable and returns from result set
    //--    colNames -> if column names has to be changed from original ones in table
    //--    sql -> what sql syntax to call
    //SHARED METHOD
    protected static JTable getTableReady(Vector<String> colNames,String sql) {
        try(PreparedStatement pstat = Datasource.conn.prepareStatement(sql);
            ResultSet rs = pstat.executeQuery()) {
            SharedTableModel stb;
            if(colNames != null) {
                stb = new SharedTableModel(rs,colNames);
            } else {
                stb = new SharedTableModel(rs);
            }

            return new JTable(stb.getTableModel());
        } catch(SQLException e) {
            return null;
        }
    }

    /**
     *
     * @param ID    -   type Integer
     * @param SQL   -   type String
     * @return boolean
     * <br><b>Description</b> : <br> Removes item from table specified by SQL syntax
     */
    //Removes item from any table uses:
    //--    ID -> by id decides what item to remove
    //--    SQL -> calls passed sql syntax
    //SHARED METHOD
    public static boolean removeTableItem(int ID, String SQL) {
        try(PreparedStatement pstat = conn.prepareStatement(SQL)) {
            pstat.setInt(1, ID);
            pstat.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }

    }


}
