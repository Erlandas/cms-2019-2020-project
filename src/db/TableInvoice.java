package db;

/**
 * @author  Erlandas Bacauskas
 * <br><b>Description</b> : <br>
 *      Meant for database Table: invoice_*
 *      Class that works directly with database
 *      This class stores all the variables describing columns and column names
 *      Also as syntax for SQL
 */

import javax.swing.*;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

public class TableInvoice {

    private static String invoiceRecordName;

    private static final String COLUMN_INVOICE_ID = "id";
    private static final String COLUMN_INVOICE_CUSTOMER_ID = "customer_id";
    private static final String COLUMN_INVOICE_PRODUCT_ID = "product_id";
    private static final String COLUMN_INVOICE_PRODUCT_QUANTITY = "quantity";

    private static String SQL_CREATE_INVOICE;
    private static String SQL_DELETE_INVOICE;
    private static String SQL_INSERT_INTO_INVOICE;
    private static String SQL_INNER_JOIN_INVOICE_TO_PRODUCT;

    /**
     *
     * @param tableName -   type String
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates SQL syntax for invoice table
     */
    public static void setInvoiceSqlSyntax(String tableName) {

        invoiceRecordName = tableName;

        SQL_CREATE_INVOICE = "CREATE TABLE IF NOT EXISTS " + tableName +" (" +
                COLUMN_INVOICE_ID + " int(10) not null auto_increment, " +
                COLUMN_INVOICE_CUSTOMER_ID + " int(10) not null, " +
                COLUMN_INVOICE_PRODUCT_ID + " int(10) not null, " +
                COLUMN_INVOICE_PRODUCT_QUANTITY + " varchar (10) not null, " +
                " PRIMARY KEY(" + COLUMN_INVOICE_ID + ", " + COLUMN_INVOICE_CUSTOMER_ID + ", " + COLUMN_INVOICE_PRODUCT_ID + "))";

        SQL_DELETE_INVOICE = "DROP TABLE IF EXISTS " + tableName;

        SQL_INSERT_INTO_INVOICE = "INSERT INTO " + tableName + " (" +
                COLUMN_INVOICE_PRODUCT_ID + ", " +
                COLUMN_INVOICE_CUSTOMER_ID + ", " +
                COLUMN_INVOICE_PRODUCT_QUANTITY + ") VALUES (?,?,?)";

        SQL_INNER_JOIN_INVOICE_TO_PRODUCT = "SELECT "+TableProduct.TABLE_PRODUCT+"."+TableProduct.COLUMN_PRODUCT_NAME+", "+
                TableProduct.TABLE_PRODUCT+"."+TableProduct.COLUMN_PRODUCT_BRAND+", "+
                TableProduct.TABLE_PRODUCT+"."+TableProduct.COLUMN_PRODUCT_DESCRIPTION+", "+
                TableProduct.TABLE_PRODUCT+"."+TableProduct.COLUMN_PRODUCT_PRICE+", "+
                tableName+"."+COLUMN_INVOICE_PRODUCT_QUANTITY+
                " FROM "+TableProduct.TABLE_PRODUCT+
                " INNER JOIN "+tableName+
                " ON "+tableName+"."+COLUMN_INVOICE_PRODUCT_ID+"="+TableProduct.TABLE_PRODUCT+"."+TableProduct.COLUMN_PRODUCT_ID;
    }

    /**
     *
     * @return boolean
     * <br><b>Description</b> : <br>
     * Creates invoice table in database
     */
    public static boolean invoiceCreate() {

        try(PreparedStatement pstat = Datasource.conn.prepareStatement(SQL_CREATE_INVOICE)) {
            pstat.executeUpdate();
            return true;
        } catch (SQLException e) { return false; }

    }

    /**
     * Deletes invoice table from database
     */
    public static void invoiceDeleteTable() {

        try (Statement statement = Datasource.conn.createStatement()) {
            statement.executeUpdate(SQL_DELETE_INVOICE);
        } catch (SQLException e) {}
    }

    /**
     *
     * @param product_id Integer
     * @param customer_id String
     * @param quantity String
     *                 <br><b>Description</b> : <br>
     * Adds item to invoice table
     */
    public static void invoiceAddItem(int product_id, int customer_id, String quantity) {

        try(PreparedStatement pstat = Datasource.conn.prepareStatement(SQL_INSERT_INTO_INVOICE)) {

            pstat.setInt(1, product_id);
            pstat.setInt(2, customer_id);
            pstat.setString(3, quantity);


            pstat.executeUpdate();
        } catch (SQLException e) {

        }


    }

    /**
     *
     * @param colNames  -   type Vector<String>
     * @return JTable
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates JTable from database table
     */
    public static JTable getInvoiceProducts(Vector<String> colNames) {
        return Datasource.getTableReady(colNames, SQL_INNER_JOIN_INVOICE_TO_PRODUCT);
    }

    /**
     *
     * @return JTable
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates JTable from database table
     */
    public static JTable getInvoiceProducts() {
        return getInvoiceProducts(null);
    }

}
