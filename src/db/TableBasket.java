package db;

/*
 * Programmer:  Erlandas Bacauskas
 * Description:
 *      Meant for database Table: basket_*
 *      Temporary place before committing purchase and creating invoice
 *      Class that works directly with database
 *      This class stores all the variables describing columns and column names
 *      Also as syntax for SQL
 */

import javax.swing.JTable;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

public class TableBasket {


    //Methods related working with customer basket
    //--    Temporary place to store item list of not made purchases

    private static final String COLUMN_CUSTOMER_BASKET_ID = "id";
    private static final String COLUMN_CUSTOMER_BASKET_PRODUCT_ID = "productid";
    private static final String COLUMN_CUSTOMER_BASKET_PRODUCT_NAME = "product_name";
    private static final String COLUMN_CUSTOMER_BASKET_PRODUCT_BRAND = "product_brand";
    private static final String COLUMN_CUSTOMER_BASKET_PRODUCT_PRICE = "price";
    private static final String COLUMN_CUSTOMER_BASKET_PRODUCT_QUANTITY = "quantity";


    private static String SQL_CREATE_BASKET;
    private static String SQL_DELETE_TABLE;
    private static String SQL_INSERT_PRODUCT;
    private static String SQL_GET_BASKET;
    private static String SQL_REMOVE_FROM_BASKET;

    /**
     *
     * @param tableName -   type String
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates SQL syntax for basket table
     */
    //initialise sql syntax using table name passed as argument
    public static void setSqlBasketCreateSqlSyntax(String tableName) {
        SQL_CREATE_BASKET = "CREATE TABLE IF NOT EXISTS " + tableName +" (" +
                COLUMN_CUSTOMER_BASKET_ID + " int(10) not null auto_increment primary key, " +
                COLUMN_CUSTOMER_BASKET_PRODUCT_ID + " int(10) not null, " +
                COLUMN_CUSTOMER_BASKET_PRODUCT_NAME + " varchar (35) not null, " +
                COLUMN_CUSTOMER_BASKET_PRODUCT_BRAND + " varchar (30), " +
                COLUMN_CUSTOMER_BASKET_PRODUCT_PRICE + " varchar (10) not null, " +
                COLUMN_CUSTOMER_BASKET_PRODUCT_QUANTITY + " varchar (10))";

        SQL_DELETE_TABLE = "DROP TABLE IF EXISTS " + tableName;

        SQL_INSERT_PRODUCT = "INSERT INTO " + tableName + "(" +
                COLUMN_CUSTOMER_BASKET_PRODUCT_ID + ", " +
                COLUMN_CUSTOMER_BASKET_PRODUCT_NAME + ", " +
                COLUMN_CUSTOMER_BASKET_PRODUCT_BRAND + ", " +
                COLUMN_CUSTOMER_BASKET_PRODUCT_PRICE + ", " +
                COLUMN_CUSTOMER_BASKET_PRODUCT_QUANTITY +
                ") VALUES (?, ?, ?, ?, ?)";

        SQL_GET_BASKET = "SELECT * FROM " + tableName;

        SQL_REMOVE_FROM_BASKET = "DELETE FROM " + tableName + " WHERE " + COLUMN_CUSTOMER_BASKET_ID + "=?";
    }

    /**
     *
     * @return boolean
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates basket table in database
     */
    public static boolean basketCreate() {

        try(PreparedStatement pstat = Datasource.conn.prepareStatement(SQL_CREATE_BASKET)) {
            pstat.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }

    }

    /**
     *
     * @param p         -   type Product
     * @param quantity  -   type Integer
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Adds product to basket table
     */
    public static void basketAddTo(Product p, String quantity) {

        try(PreparedStatement pstat = Datasource.conn.prepareStatement(SQL_INSERT_PRODUCT)) {

            pstat.setInt(1,p.getID());
            pstat.setString(2,p.getName());
            pstat.setString(3,p.getBrand());
            pstat.setString(4,p.getPrice());
            pstat.setString(5,quantity);

            pstat.executeUpdate();
        } catch (SQLException e) {

        }

    }

    /**
     *
     * @param id    -   type Integer
     * @return boolean
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      removes product from basket table using matching ID
     */
    public static boolean basketRemoveItem(int id) {
        return Datasource.removeTableItem(id, SQL_REMOVE_FROM_BASKET);
    }

    /**
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      deletes basket table in database
     */
    public static void basketDeleteTable() {

        try (Statement statement = Datasource.conn.createStatement()) {
            statement.executeUpdate(SQL_DELETE_TABLE);
        } catch (SQLException e) {

        }
    }

    /**
     *
     * @param colNames  -   type Vector<String>
     * @return JTable
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates JTable from database table
     */
    public static JTable getBasketTableReady(Vector<String> colNames) {
        return Datasource.getTableReady(colNames, SQL_GET_BASKET);
    }

    /**
     *
     * @return JTable
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates JTable from database table
     */
    public static JTable getBasketTableReady() {
        return getBasketTableReady(null);
    }
}
