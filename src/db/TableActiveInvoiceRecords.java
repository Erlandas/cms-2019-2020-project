package db;

/**
 * @author  Erlandas Bacauskas
 * <br><b>Description</b> : <br>
 *      Meant for database Table: active_invoice_records
 *      Class that works directly with database
 *      This class stores all the variables describing columns and column names
 *      Also as syntax for SQL
 */

import javax.swing.*;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Vector;

public class TableActiveInvoiceRecords {

    private static final String TABLE_INVOICE_ACTIVE_RECORDS = "active_invoice_records";
    private static final String COLUMN_INVOICE_ID = "id";
    private static final String COLUMN_INVOICE_RECORD_NAME = "invoice_table_name";
    private static final String COLUMN_INVOICE_DATE_CREATED = "date_created";


    private static final String SQL_CREATE_INVOICE_RECORD_TABLE = "CREATE TABLE IF NOT EXISTS " +
            TABLE_INVOICE_ACTIVE_RECORDS + " (" +
            COLUMN_INVOICE_ID + " int(10) not null auto_increment PRIMARY KEY, " +
            COLUMN_INVOICE_RECORD_NAME + " varchar (18) not null, " +
            COLUMN_INVOICE_DATE_CREATED + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP, UNIQUE ("+ COLUMN_INVOICE_RECORD_NAME +"))";

    private static final String SQL_ADD_INVOICE_RECORD = "INSERT INTO " + TABLE_INVOICE_ACTIVE_RECORDS + " (" +
            COLUMN_INVOICE_RECORD_NAME + ") VALUES (?)";

    private static final String SQL_DELETE_INVOICE_RECORD  = "DELETE FROM " +
            TABLE_INVOICE_ACTIVE_RECORDS + " WHERE " + COLUMN_INVOICE_ID + "=?";

    private static final String SQL_GET_ACTIVE_INVOICES = "SELECT * FROM " + TABLE_INVOICE_ACTIVE_RECORDS;

    //Using static initialisation block
    static {
        try(PreparedStatement pstat = Datasource.conn.prepareStatement(SQL_CREATE_INVOICE_RECORD_TABLE)){
            pstat.executeUpdate();
        } catch (SQLException e) {
        }
    }

    /**
     *
     * @param invoiceName   -   type String
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Adds invoice record to active invoice records table
     */
    public static void invoiceAddRecord(String invoiceName) {

        try(PreparedStatement pstat = Datasource.conn.prepareStatement(SQL_ADD_INVOICE_RECORD)) {

            pstat.setString(1,invoiceName);
            pstat.executeUpdate();
        } catch (SQLException e) {
        }
    }

    /**
     *
     * @param ID Integer
     * @return boolean
     * <br><b>Description</b> : <br>
     * Removes record from table
     */
    public static boolean invoiceRemoveRecord(int ID) {
        return Datasource.removeTableItem(ID, SQL_DELETE_INVOICE_RECORD);
    }

    /**
     *
     * @param colNames  -   type Vector<String>
     * @return JTable
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates JTable from database table
     */
    public static JTable getActiveInvoiceTableReady(Vector<String> colNames) {
        return Datasource.getTableReady(colNames, SQL_GET_ACTIVE_INVOICES);
    }

    /**
     *
     * @return JTable
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates JTable from database table
     */
    public static JTable getActiveInvoiceTableReady() {
        return getActiveInvoiceTableReady(null);
    }


}
