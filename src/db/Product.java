package db;

/**
 * @author  Erlandas Bacauskas
 * <br><b>Description</b> : <br>
 *      Object Class that creates
 *      Object instance of a product
 *      used to retrieve products from database as an objects
 */

public class Product {

    private int ID;
    private String name;
    private String brand;
    private String description;
    private String price;
    private String quantity;
    private String imgLocation;

    /**
     *
     * @param ID            -   type Integer
     * @param name          -   type String
     * @param brand         -   type String
     * @param description   -   type String
     * @param price         -   type String
     * @param quantity      -   type String
     * @param imgLocation   -   type String
     * <br><b>Description</b> : <br> constructor that creates Product type object from passed arguments
     */
    public Product(int ID, String name, String brand, String description, String price, String quantity, String imgLocation) {
        this.ID = ID;
        this.name = name;
        this.brand = brand;
        this.description = description;
        this.price = price;
        this.quantity = quantity;
        this.imgLocation = imgLocation;
    }

    /**
     * Default constructor
     */
    public Product() {

    }

    /**
     *
     * @param ID    -   type Integer
     */
    public void setID(int ID) {
        this.ID = ID;
    }

    /**
     *
     * @param name  -   type String
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @param brand  -   type String
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     *
     * @param description   -   type String
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @param price -   type String
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     *
     * @param quantity  -   type String
     */
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    /**
     *
     * @param imgLocation   -   type String
     */
    public void setImgLocation(String imgLocation) {
        this.imgLocation = imgLocation;
    }

    /**
     *
     * @return Integer
     */
    public int getID() {
        return ID;
    }

    /**
     *
     * @return  String
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return  String
     */
    public String getBrand() {
        return brand;
    }

    /**
     *
     * @return  String
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @return  String
     */
    public String getPrice() {
        return price;
    }

    /**
     *
     * @return  String
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     *
     * @return  String
     */
    public String getImgLocation() {
        return imgLocation;
    }

    /**
     *
     * @return  String
     */
    @Override
    public String toString() {
        return "Product{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", description='" + description + '\'' +
                ", price='" + price + '\'' +
                ", quantity='" + quantity + '\'' +
                ", imgLocation='" + imgLocation + '\'' +
                '}';
    }
}
