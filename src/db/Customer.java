package db;

/**
 * @author  Erlandas Bacauskas
 * Description:
 *      Object Class that creates
 *      Object instance of a customer
 *      used to retrieve customers from database as an objects
 */

public class Customer {

    private int ID;
    private String email;
    private String firstName;
    private String lastName;
    private String addressLine1;
    private String addressLine2;
    private String town;
    private String county;
    private String country;
    private String password;
    private String phone;
    private String gender;
    private int age;

    /**
     * <br><b>Description</b> : <br>
     * Constructor that creates Customer type object<br>
     * @param ID            -   type Integer
     * @param email         -   type String
     * @param firstName     -   type String
     * @param lastName      -   type String
     * @param addressLine1  -   type String
     * @param addressLine2  -   type String
     * @param town          -   type String
     * @param county        -   type String
     * @param country       -   type String
     * @param password      -   type String
     * @param phone         -   type String
     * @param gender        -   type String
     * @param age           -   type Integer
     *
     */
    public Customer(
            int ID, String email, String firstName, String lastName, String addressLine1, String addressLine2,
            String town, String county, String country, String password, String phone, String gender, int age) {
        this.ID = ID;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.town = town;
        this.county = county;
        this.country = country;
        this.password = password;
        this.phone = phone;
        this.gender = gender;
        this.age = age;
    }

    /**
     * <br><b>Description</b> : <br>
     * Default constructor
     */
    public Customer() {

    }

    /**
     *
     * @param ID    -   type Integer
     */
    public void setID(int ID) {
        this.ID = ID;
    }

    /**
     *
     * @param email   -   type String
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @param firstName -   type String
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @param lastName  -   type String
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @param addressLine1  -   type String
     */
    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    /**
     *
     * @param addressLine2  -   type String
     */
    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    /**
     *
     * @param town  -   type String
     */
    public void setTown(String town) {
        this.town = town;
    }

    /**
     *
     * @param county    -   type String
     */
    public void setCounty(String county) {
        this.county = county;
    }

    /**
     *
     * @param country   -   type String
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     *
     * @param password  -   type String
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @param phone -   type String
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @param gender    -   type String
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     *
     * @param age   -   type Integer
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     *
     * @return Integer
     */
    public int getID() {
        return ID;
    }

    /**
     *
     * @return String
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @return  String
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @return  String
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @return  String
     */
    public String getAddressLine1() {
        return addressLine1;
    }

    /**
     *
     * @return  String
     */
    public String getAddressLine2() {
        return addressLine2;
    }

    /**
     *
     * @return  String
     */
    public String getTown() {
        return town;
    }

    /**
     *
     * @return  String
     */
    public String getCounty() {
        return county;
    }

    /**
     *
     * @return  String
     */
    public String getCountry() {
        return country;
    }

    /**
     *
     * @return  String
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @return  String
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @return  String
     */
    public String getGender() {
        return gender;
    }

    /**
     *
     * @return  String
     */
    public int getAge() {
        return age;
    }

    /**
     *
     * @return  String
     */
    @Override
    public String toString() {
        return "Customer{" +
                "ID=" + ID +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", addressLine1='" + addressLine1 + '\'' +
                ", addressLine2='" + addressLine2 + '\'' +
                ", town='" + town + '\'' +
                ", county='" + county + '\'' +
                ", country='" + country + '\'' +
                ", password='" + password + '\'' +
                ", phone='" + phone + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                '}';
    }
}
