package db;

/**
 * @author  Erlandas Bacauskas
 * <br><b>Description</b> : <br>
 *      Meant for database Table: product
 *      Class that works directly with database
 *      This class stores all the variables describing columns and column names
 *      Also as syntax for SQL
 */

import javax.swing.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

public class TableProduct {

    protected static final String TABLE_PRODUCT = "product";
    protected static final String COLUMN_PRODUCT_ID = "productid";
    protected static final String COLUMN_PRODUCT_NAME = "product_name";
    protected static final String COLUMN_PRODUCT_BRAND = "product_brand";
    protected static final String COLUMN_PRODUCT_DESCRIPTION = "description";
    protected static final String COLUMN_PRODUCT_PRICE = "price";
    private static final String COLUMN_PRODUCT_QUANTITY = "quantity";
    private static final String COLUMN_PRODUCT_IMG_LOCATION = "img_location";

    private static final String SQL_CREATE_PRODUCT = "INSERT INTO " + TABLE_PRODUCT + " (" +
            COLUMN_PRODUCT_NAME + ", " + COLUMN_PRODUCT_BRAND + ", " +
            COLUMN_PRODUCT_DESCRIPTION + ", " + COLUMN_PRODUCT_PRICE + ", " +
            COLUMN_PRODUCT_QUANTITY + ", " + COLUMN_PRODUCT_IMG_LOCATION + ") " +
            "values(?, ?, ?, ?, ?, ?)";

    private static final String SQL_GET_PRODUCTS = "SELECT * FROM " + TABLE_PRODUCT;

    private static final String SQL_GET_PRODUCT ="SELECT * FROM " +TABLE_PRODUCT + " WHERE " +COLUMN_PRODUCT_ID + "=?";

    private static final String SQL_REMOVE_PRODUCT = "DELETE FROM " + TABLE_PRODUCT + " WHERE " + COLUMN_PRODUCT_ID + "=?";

    private static final String SQL_UPDATE_PRODUCT = "UPDATE " + TABLE_PRODUCT + " SET " +
            COLUMN_PRODUCT_NAME + "=?, " + COLUMN_PRODUCT_BRAND + "=?, " +
            COLUMN_PRODUCT_DESCRIPTION + "=?, " + COLUMN_PRODUCT_PRICE + "=?, " +
            COLUMN_PRODUCT_QUANTITY + "=?, " + COLUMN_PRODUCT_IMG_LOCATION + "=? " +
            "WHERE " + COLUMN_PRODUCT_ID + "=?";


    /**
     *
     * @param name          -   type String
     * @param brand         -   type String
     * @param description   -   type String
     * @param price         -   type String
     * @param quantity      -   type String
     * @param location      -   type String
     * @return boolean
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates product type entry in product table in the database
     */
    public static boolean createProduct(
            String name, String brand, String description, String price, String quantity, String location) {

        try(PreparedStatement pstat = Datasource.conn.prepareStatement(SQL_CREATE_PRODUCT)) {

            pstat.setString(1,name);
            pstat.setString(2,brand);
            pstat.setString(3,description);
            pstat.setString(4,price);
            pstat.setString(5,quantity);
            pstat.setString(6,location);

            pstat.executeUpdate();


            return true;

        } catch (SQLException e) {
            return false;
        }
    }

    /**
     *
     * @return ArrayList<Product>
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Puts all Product object data from product table to list
     */
    public static ArrayList<Product> getProducts() {

        try (PreparedStatement pstat = Datasource.conn.prepareStatement(SQL_GET_PRODUCTS);
             ResultSet rs = pstat.executeQuery()) {

            ArrayList<Product> products = new ArrayList();
            while (rs.next()) {
                Product product = new Product();
                product.setID((int)rs.getObject(1));
                product.setName((String)rs.getObject(2));
                product.setBrand((String)rs.getObject(3));
                product.setDescription((String)rs.getObject(4));
                product.setPrice((String)rs.getObject(5));
                product.setQuantity((String)rs.getObject(6));
                product.setImgLocation((String)rs.getObject(7));
                products.add(product);
            }
            return products;
        } catch (SQLException e) {
            return null;
        }

    }

    /**
     *
     * @param ID    -   type Integer
     * @return boolean
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      removes product from product table using matching ID
     */
    public static boolean removeProduct(int ID) {
        return Datasource.removeTableItem(ID, SQL_REMOVE_PRODUCT);
    }

    /**
     *
     * @param name          -   type String
     * @param brand         -   type String
     * @param description   -   type String
     * @param price         -   type String
     * @param quantity      -   type String
     * @param location      -   type String
     * @param iD            -   type Integer
     * @return boolean
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      updates existing product record in product table
     */
    public static boolean updateProduct(
            String name, String brand, String description, String price, String quantity, String location, int iD) {

        try(PreparedStatement pstat = Datasource.conn.prepareStatement(SQL_UPDATE_PRODUCT)) {

            pstat.setString(1,name);
            pstat.setString(2,brand);
            pstat.setString(3,description);
            pstat.setString(4,price);
            pstat.setString(5,quantity);
            pstat.setString(6,location);
            pstat.setInt(7,iD);

            pstat.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }

    }

    /**
     *
     * @param colNames  -   type Vector<String>
     * @return JTable
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates JTable from database table
     */
    public static JTable getProductTableReady(Vector<String> colNames) {
        return Datasource.getTableReady(colNames, SQL_GET_PRODUCTS);
    }

    /**
     *
     * @return JTable
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates JTable from database table
     */
    public static JTable getProductTableReady() {
       return getProductTableReady(null);
    }


}
