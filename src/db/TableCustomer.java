package db;

/**
 * @author  Erlandas Bacauskas
 * <br><b>Description</b> : <br>
 *      Meant for database Table: customer
 *      Class that works directly with database
 *      This class stores all the variables describing columns and column names
 *      Also as syntax for SQL
 */

import javax.swing.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

public class TableCustomer {

    //Data for CUSTOMER table column names as CONSTANTS for SQL statements
    private static final String TABLE_CUSTOMER = "customer";
    private static final String COLUMN_CUSTOMER_ID = "Customer_ID";
    private static final String COLUMN_CUSTOMER_EMAIL = "email";
    private static final String COLUMN_CUSTOMER_FIRST_NAME = "first_name";
    private static final String COLUMN_CUSTOMER_LAST_NAME = "last_name";
    private static final String COLUMN_CUSTOMER_ADDRESS_LINE_ONE = "address_line1";
    private static final String COLUMN_CUSTOMER_ADDRESS_LINE_TWO = "address_line2";
    private static final String COLUMN_CUSTOMER_TOWN = "town";
    private static final String COLUMN_CUSTOMER_COUNTY = "county";
    private static final String COLUMN_CUSTOMER_COUNTRY = "country";
    private static final String COLUMN_CUSTOMER_PASSWORD = "password";
    private static final String COLUMN_CUSTOMER_PHONE = "phone";
    private static final String COLUMN_CUSTOMER_GENDER = "gender";
    private static final String COLUMN_CUSTOMER_AGE = "age";
    private static final String COLUMN_CUSTOMER_ACTIVE_FROM = "active_from";

    //SQL prepared statements
    private static final String SQL_SELECT_CUSTOMER =
            "SELECT " + COLUMN_CUSTOMER_EMAIL + ", " + COLUMN_CUSTOMER_PASSWORD +
                    " FROM " + TABLE_CUSTOMER + " where " + COLUMN_CUSTOMER_EMAIL +
                    "=? and " + COLUMN_CUSTOMER_PASSWORD + "=?";

    private static final String SQL_EMAIL_CONFIRM = "SELECT * FROM " +
            TABLE_CUSTOMER + " WHERE " +
            COLUMN_CUSTOMER_EMAIL + "=?";

    private static final String SQL_CREATE_CUSTOMER = "INSERT INTO " + TABLE_CUSTOMER + " (" +
            COLUMN_CUSTOMER_EMAIL + ", " + COLUMN_CUSTOMER_FIRST_NAME + ", " +
            COLUMN_CUSTOMER_LAST_NAME + ", " + COLUMN_CUSTOMER_ADDRESS_LINE_ONE + ", " +
            COLUMN_CUSTOMER_ADDRESS_LINE_TWO + ", " + COLUMN_CUSTOMER_TOWN + ", " +
            COLUMN_CUSTOMER_COUNTY + ", " + COLUMN_CUSTOMER_COUNTRY + ", " +
            COLUMN_CUSTOMER_PASSWORD + ", " + COLUMN_CUSTOMER_PHONE + ", " +
            COLUMN_CUSTOMER_GENDER + ", " + COLUMN_CUSTOMER_AGE + ") " +
            "values(?,?,?,?,?,?,?,?,?,?,?,?)";

    private static final String SQL_GET_CUSTOMERS = "SELECT * FROM " + TABLE_CUSTOMER;

    private static final String SQL_GET_CUSTOMERS_TABLE_DATA = "SELECT " + COLUMN_CUSTOMER_FIRST_NAME + ", " +
            COLUMN_CUSTOMER_LAST_NAME + ", " + COLUMN_CUSTOMER_EMAIL + ", " +
            COLUMN_CUSTOMER_ACTIVE_FROM + " FROM " + TABLE_CUSTOMER;

    private static final String SQL_UPDATE_CUSTOMER_PASSWORD = "UPDATE " + TABLE_CUSTOMER + " SET " +
            COLUMN_CUSTOMER_PASSWORD + "=? WHERE " + COLUMN_CUSTOMER_ID + "=?";

    private static final String SQL_REMOVE_CUSTOMER = "DELETE FROM " + TABLE_CUSTOMER + " WHERE " + COLUMN_CUSTOMER_ID + "=?";

    private static final String SQL_GET_ACTIVE_CUSTOMER = "SELECT * FROM " + TABLE_CUSTOMER +
            " WHERE " + COLUMN_CUSTOMER_EMAIL + "=?";

    private static final String SQL_GET_CUSTOMER_BY_ID = "SELECT * FROM " + TABLE_CUSTOMER +
            " WHERE " + COLUMN_CUSTOMER_ID + "=?";

    /**
     *
     * @param ID            -   type Integer
     * @param newPassword   -   type String
     * @return boolean
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Updates customer password in customer table entry
     */
    public static boolean updateCustomerPassword(int ID, String newPassword) {

        try(PreparedStatement pstat = Datasource.conn.prepareStatement(SQL_UPDATE_CUSTOMER_PASSWORD)) {
            pstat.setString(1,newPassword);
            pstat.setInt(2,ID);
            pstat.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    /**
     *
     * @param email -   type String
     * @param password  -   type String
     * @return boolean
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      checks if user is present in database table
     */
    //Method that checks if user is in database
    //--    Returns boolean value that indicates if user is present
    //--    In the database
    //--    Preventing unauthorised access
    public static boolean isUserPresent(String email, String password) {

        try (PreparedStatement pstat = Datasource.conn.prepareStatement(SQL_SELECT_CUSTOMER)) {

            pstat.setString(1,email);
            pstat.setString(2,password);
            ResultSet rs = pstat.executeQuery();
            if(rs.next()) {
                //JOptionPane.showMessageDialog(null,"You have successfully logged in");
                rs.close();
                return true;
            }
            rs.close();
            return false;
        } catch (SQLException e) {
            return false;
        }
    }

    /**
     *
     * @param colNames  -   type Vector<String>
     * @return JTable
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates JTable from database table
     */
    public static JTable getCustomerTableReady(Vector<String> colNames) {
        return Datasource.getTableReady(colNames, SQL_GET_CUSTOMERS_TABLE_DATA);
    }

    /**
     *
     * @return JTable
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates JTable from database table
     */
    public static JTable getCustomerTableReady() {
        return getCustomerTableReady(null);
    }

    /**
     *
     * @param email         -   type String
     * @param firstName     -   type String
     * @param lastName      -   type String
     * @param addL1         -   type String
     * @param addL2         -   type String
     * @param town          -   type String
     * @param county        -   type String
     * @param country       -   type String
     * @param password      -   type String
     * @param phone         -   type String
     * @param gender        -   type String
     * @param age           -   type Integer
     * @return boolean
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      creates user type entry in the database table of users
     */
    public static boolean createCustomer(
            String email, String firstName, String lastName, String addL1, String addL2, String town,
            String county, String country, String password, String phone, String gender, int age){

        try (PreparedStatement pstat = Datasource.conn.prepareStatement(SQL_CREATE_CUSTOMER)) {

            pstat.setString(1,email);
            pstat.setString(2,firstName);
            pstat.setString(3,lastName);
            pstat.setString(4,addL1);
            pstat.setString(5,addL2);
            pstat.setString(6,town);
            pstat.setString(7,county);
            pstat.setString(8,country);
            pstat.setString(9,password);
            pstat.setString(10,phone);
            pstat.setString(11,gender);
            pstat.setInt(12,age);

            pstat.executeUpdate();
            return true;

        } catch (SQLException e) {
            return false;
        }

    }

    /**
     *
     * @param email -   type string
     * @return boolean
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      checks if email is taken
     */
    //Method that checks if email is already taken
    //--    Returns boolean that indicates
    //--    TRUE -> email is available
    //--    FALSE -> email is !available
    public static boolean isEmailPresent(String email) {
        try (PreparedStatement pstat = Datasource.conn.prepareStatement(SQL_EMAIL_CONFIRM)) {
            pstat.setString(1,email);
            ResultSet rs = pstat.executeQuery();
            if(rs.next()) {
                JOptionPane.showMessageDialog(null,"Email Address provided is already taken");
                rs.close();
                return false;
            }
            rs.close();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    /**
     *
     * @return ArrayList<Customer>
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Puts all Customer object data from customer table to list
     */
    public static ArrayList<Customer> getCustomers() {

        try (PreparedStatement pstat = Datasource.conn.prepareStatement(SQL_GET_CUSTOMERS);
             ResultSet rs = pstat.executeQuery()) {

            ArrayList<Customer> customers = new ArrayList();
            while (rs.next()) {

                Customer c = new Customer();
                c.setID((int)rs.getObject(1));
                c.setEmail((String)rs.getObject(2));
                c.setFirstName((String)rs.getObject(3));
                c.setLastName((String)rs.getObject(4));
                c.setAddressLine1((String)rs.getObject(5));
                c.setAddressLine2((String)rs.getObject(6));
                c.setTown((String)rs.getObject(7));
                c.setCounty((String)rs.getObject(8));
                c.setCountry((String)rs.getObject(9));
                c.setPassword((String)rs.getObject(10));
                c.setPhone((String)rs.getObject(11));
                c.setGender((String)rs.getObject(12));
                c.setAge((int)rs.getObject(13));
                customers.add(c);

            }

            return customers;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,e.getMessage());
            return null;
        }
    }

    /**
     *
     * @param ID    -   type Integer
     * @return boolean
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      removes user from customers table using matching ID
     */
    public static boolean removeCustomer(int ID) {
        return Datasource.removeTableItem(ID, SQL_REMOVE_CUSTOMER);
    }

    /**
     *
     * @param email -   type String
     * @return Customer
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Retrieves currently active customer
     */
    public static Customer getActiveCustomer(String email) {

        try(PreparedStatement pstat = Datasource.conn.prepareStatement(SQL_GET_ACTIVE_CUSTOMER)) {
            pstat.setString(1,email);
            ResultSet rs = pstat.executeQuery();
            if(rs.next()) {

                Customer c = new Customer();
                c.setID((int)rs.getObject(1));
                c.setEmail((String)rs.getObject(2));
                c.setFirstName((String)rs.getObject(3));
                c.setLastName((String)rs.getObject(4));
                c.setAddressLine1((String)rs.getObject(5));
                c.setAddressLine2((String)rs.getObject(6));
                c.setTown((String)rs.getObject(7));
                c.setCounty((String)rs.getObject(8));
                c.setCountry((String)rs.getObject(9));
                c.setPassword((String)rs.getObject(10));
                c.setPhone((String)rs.getObject(11));
                c.setGender((String)rs.getObject(12));
                c.setAge((int)rs.getObject(13));

                return c;
            }

            return null;

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,e.getMessage(),"MESSAGE",JOptionPane.WARNING_MESSAGE);
        }
        return null;
    }

    /**
     *
     * @return Customer
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Retrieves customer by ID
     */
    public static Customer getCustomerByID(int ID) {

        try(PreparedStatement pstat = Datasource.conn.prepareStatement(SQL_GET_CUSTOMER_BY_ID)) {
            pstat.setInt(1,ID);
            ResultSet rs = pstat.executeQuery();
            if(rs.next()) {

                Customer c = new Customer();
                c.setID((int)rs.getObject(1));
                c.setEmail((String)rs.getObject(2));
                c.setFirstName((String)rs.getObject(3));
                c.setLastName((String)rs.getObject(4));
                c.setAddressLine1((String)rs.getObject(5));
                c.setAddressLine2((String)rs.getObject(6));
                c.setTown((String)rs.getObject(7));
                c.setCounty((String)rs.getObject(8));
                c.setCountry((String)rs.getObject(9));
                c.setPassword((String)rs.getObject(10));
                c.setPhone((String)rs.getObject(11));
                c.setGender((String)rs.getObject(12));
                c.setAge((int)rs.getObject(13));

                return c;
            }

            return null;

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,e.getMessage(),"MESSAGE",JOptionPane.WARNING_MESSAGE);
        }
        return null;
    }
}
