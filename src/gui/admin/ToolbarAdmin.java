package gui.admin;

/**
 * @author  Erlandas Bacauskas
 * Description:
 *      Class that creates Toolbar with an options
 */

import gui.shared.SharedFunctions;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

public class ToolbarAdmin {

    private static JPanel panelToolbar;
    protected static JButton btnProduct;
    protected static JButton btnCustomers;
    protected static JButton btnOrder;
    private static LineBorder lb;
    private static Dimension dim;

    /**
     * @return type JPanel
     * <br><b>Description</b> : <br>
     *      Does initial setup of top toolbar with a buttons that navigates through panels
     */

    public static JPanel panelToolbar() {

        panelToolbar = new JPanel();
        lb = new LineBorder(Color.WHITE);
        dim = new Dimension(160,25);

        btnProduct = new JButton("Product");
        btnCustomers = new JButton("Customers");
        btnOrder = new JButton("Invoices");

        btnProduct.setBorder(lb);
        btnProduct.setPreferredSize(dim);
        btnProduct.setBackground(Color.LIGHT_GRAY);

        btnCustomers.setBorder(lb);
        btnCustomers.setPreferredSize(dim);
        btnCustomers.setBackground(Color.LIGHT_GRAY);

        btnOrder.setBorder(lb);
        btnOrder.setPreferredSize(dim);
        btnOrder.setBackground(Color.LIGHT_GRAY);

        SharedFunctions.buttonMouseEvents(btnProduct);
        SharedFunctions.buttonMouseEvents(btnCustomers);
        SharedFunctions.buttonMouseEvents(btnOrder);

        panelToolbar.setLayout(new FlowLayout(FlowLayout.RIGHT));

        panelToolbar.add(btnProduct);
        panelToolbar.add(btnCustomers);
        panelToolbar.add(btnOrder);
        panelToolbar.setBackground(Color.LIGHT_GRAY);
        return panelToolbar;

    }
}
