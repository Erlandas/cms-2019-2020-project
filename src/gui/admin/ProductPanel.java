package gui.admin;

/**
 * @author  Erlandas Bacauskas
 * Description:
 *      This class holds all the configuration for the products window
 *      Including all the components
 */

import controller.DBControls;
import db.Product;
import gui.shared.SharedFunctions;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Vector;

public class ProductPanel {

    private JPanel panelBtns;
    private JButton btnCreate;
    private JButton btnDelete;
    private JButton btnUpdate;
    private LineBorder lb;
    private Dimension dim;

    private JPanel panelGetProduct;
    private JTextField textName;
    private JTextField textBrand;
    private JTextArea textDescription;
    private JTextField textPrice;
    private JTextField textQuantity;
    private JTextField textImgLocation;

    private JLabel labelName;
    private JLabel labelBrand;
    private JLabel labelDescription;
    private JLabel labelPrice;
    private JLabel labelQuantity;
    private JLabel labelImgLocation;
    private JLabel labelID;
    private Box box;

    private JPanel panelMain;
    private JPanel panelShowProductTable;
    private JPanel panelGetDetails;
    private GridBagConstraints gc;

    private JComboBox comboID;
    private ArrayList<Product> listID;

    private String stName;
    private String stBrand;
    private String stDescription;
    private String stPrice;
    private String stQuantity;
    private String stImgLocation;
    private int id;
    private JTable table;
    private Vector<String> colNames;
    private JScrollPane pane;

    /**
     *
     * @return product panel    -   type JPanel
     * <br><b>Description</b> : <br>
     *      Constructor that does initial setup  for product panel by calling private methods for :
     *      1.  Setup table
     *      2.  Setup panel to get details
     *      3.  Add controls to components
     */

    public  JPanel panelProduct() {

        panelMain = new JPanel(new BorderLayout());
        setupJTable();
        setupPanelGetDetails();
        controls();
        panelShowProductTable = new JPanel(new BorderLayout());
        panelShowProductTable.setBackground(Color.WHITE);
        panelShowProductTable.add(pane,BorderLayout.CENTER);
        panelMain.add(panelGetDetails,BorderLayout.WEST);
        panelMain.add(panelShowProductTable,BorderLayout.CENTER);
        panelMain.setBackground(Color.WHITE);
        return panelMain;
    }

    private  void controls() {

        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                int row = table.getSelectedRow();
                Product pr = listID.get(row);
                setFieldText(pr.getName(),pr.getBrand(),pr.getDescription(),pr.getPrice(),pr.getQuantity(),pr.getImgLocation());
                comboID.setSelectedIndex(row+1);
            }
        });

        comboID.addActionListener(al -> {
            int i = comboID.getSelectedIndex()-1;
            if(i == -1) {
                setFieldText("","","","","","");
                return;
            }
            Product pr = listID.get(i);
            setFieldText(pr.getName(),pr.getBrand(),pr.getDescription(),pr.getPrice(),pr.getQuantity(),pr.getImgLocation());

        });

        btnCreate.addActionListener(al -> {

            stName = textName.getText();
            if(!isFieldNotEmpty(stName,labelName)) return;

            stBrand = textBrand.getText();
            if(!isFieldNotEmpty(stBrand,labelBrand)) return;

            stDescription = textDescription.getText();

            stPrice = textPrice.getText();
            if(!isFieldNotEmpty(stPrice,labelPrice)) return;
            try {
                Double.parseDouble(stPrice);
            } catch(Exception e) {
                labelPrice.setForeground(Color.RED);
                return;
            }

            stQuantity = textQuantity.getText();
            if(!isFieldNotEmpty(stQuantity,labelQuantity)) return;

            try {
                Integer.parseInt(stQuantity);
            } catch(Exception e) {
                labelQuantity.setForeground(Color.red);
                return;
            }

            stImgLocation = textImgLocation.getText();

            if(DBControls.createProduct(stName,stBrand,stDescription,stPrice,stQuantity,stImgLocation)) {
                setupJTable();
                SharedFunctions.refreshTable(panelShowProductTable,pane);
                ToolbarAdmin.btnProduct.doClick();
                return;
            }
            JOptionPane.showMessageDialog(null,
                    "Item '" + stName + "' Couldn't Be Created",
                    "item Created",
                    JOptionPane.WARNING_MESSAGE);



        });

        btnUpdate.addActionListener(al -> {
            int index = comboID.getSelectedIndex()-1;
            if(index == -1) {
                labelID.setForeground(Color.RED);
                return;
            }

            stName = textName.getText();
            if(!isFieldNotEmpty(stName,labelName)) return;

            stBrand = textBrand.getText();
            if(!isFieldNotEmpty(stBrand,labelBrand)) return;

            stDescription = textDescription.getText();

            stPrice = textPrice.getText();
            if(!isFieldNotEmpty(stPrice,labelPrice)) return;
            try {
                Double.parseDouble(stPrice);
            } catch(Exception e) {
                labelPrice.setForeground(Color.RED);
                return;
            }

            stQuantity = textQuantity.getText();
            if(!isFieldNotEmpty(stQuantity,labelQuantity)) return;

            try {
                Integer.parseInt(stQuantity);
            } catch(Exception e) {
                labelQuantity.setForeground(Color.red);
                return;
            }

            stImgLocation = textImgLocation.getText();

            int iD = listID.get(index).getID();
            if(DBControls.updateProduct(stName,stBrand,stDescription,stPrice,stQuantity,stImgLocation,iD)) {
                setupJTable();
                SharedFunctions.refreshTable(panelShowProductTable,pane);
                ToolbarAdmin.btnProduct.doClick();
                return;
            }
            JOptionPane.showMessageDialog(null,
                    "Item With ID '" + iD  + "' Couldn't Be Updated",
                    "Warning",
                    JOptionPane.WARNING_MESSAGE);


        });

        btnDelete.addActionListener(al ->{
            id = (int)comboID.getSelectedItem();
            if(id == 0) {
                labelID.setForeground(Color.red);
                return;
            } else if (DBControls.removeProduct(id)) {
                setupJTable();
                SharedFunctions.refreshTable(panelShowProductTable,pane);
                ToolbarAdmin.btnProduct.doClick();
                return;
            }
            JOptionPane.showMessageDialog(null,"Product With ID '" + id + "' Couldn't Be Removed");
        });
    }

    //Checks if required fields is provided with data
    private  boolean isFieldNotEmpty(String fieldName, JLabel fieldLabel){
        if(fieldName.isEmpty()) {
            fieldLabel.setForeground(Color.red);
            return false;
        }
        return true;
    }

    private  void setFieldText(String name, String brand, String desc, String price, String quantity, String imgLoc) {
        textName.setText(name);
        textBrand.setText(brand);
        textDescription.setText(desc);
        textPrice.setText(price);
        textQuantity.setText(quantity);
        textImgLocation.setText(imgLoc);
    }

    private  void setupIDCombo() {
        comboID = new JComboBox();
        DefaultComboBoxModel comboIDModel = new DefaultComboBoxModel();
        listID = DBControls.getProducts();
        comboIDModel.addElement(0);
        for (int i = 0 ; i < listID.size() ; i++) {
            Product pr = listID.get(i);
            comboIDModel.addElement(pr.getID());
        }
        comboID.setModel(comboIDModel);
        comboID.setPreferredSize(new Dimension(100,25));
        comboIDModel.setSelectedItem(0);
    }

    private  void setupPanelGetDetails() {
        setupIDCombo();
        setupPanelBtns();
        setupPanelGetDetailsInitialiseComponents();
        setupPanelGetDetailsLayoutComponents();

        panelGetDetails = new JPanel(new BorderLayout());
        panelGetDetails.setBackground(Color.WHITE);
        panelGetDetails.setPreferredSize(new Dimension(250,600));
        panelGetDetails.add(panelGetProduct, BorderLayout.CENTER);
        panelGetDetails.add(panelBtns, BorderLayout.SOUTH);
    }

    private  void setupPanelGetDetailsLayoutComponents() {
        gc = new GridBagConstraints();
        gc.fill = GridBagConstraints.NONE;

        SharedFunctions.gridLayoutAddComponent(panelGetProduct,gc,labelName,0,0,1,0.1);
        SharedFunctions.gridLayoutAddComponent(panelGetProduct,gc,textName,1,0,1, 0.1);

        SharedFunctions.gridLayoutAddComponent(panelGetProduct,gc,labelBrand,0,1,1,0.1);
        SharedFunctions.gridLayoutAddComponent(panelGetProduct,gc,textBrand,1,1,1, 0.1);

        SharedFunctions.gridLayoutAddComponent(panelGetProduct,gc,labelDescription,0,2,1,0.1);
        SharedFunctions.gridLayoutAddComponent(panelGetProduct,gc,box,1,2,1, 0.1);

        SharedFunctions.gridLayoutAddComponent(panelGetProduct,gc,labelPrice,0,3,1,0.1);
        SharedFunctions.gridLayoutAddComponent(panelGetProduct,gc,textPrice,1,3,1, 0.1);

        SharedFunctions.gridLayoutAddComponent(panelGetProduct,gc,labelQuantity,0,4,1,0.1);
        SharedFunctions.gridLayoutAddComponent(panelGetProduct,gc,textQuantity,1,4,1, 0.1);

        SharedFunctions.gridLayoutAddComponent(panelGetProduct,gc,labelImgLocation,0,5,1,2);
        SharedFunctions.gridLayoutAddComponent(panelGetProduct,gc,textImgLocation,1,5,1, 2);

        SharedFunctions.gridLayoutAddComponent(panelGetProduct,gc,labelID,0,6,1,0.1);
        SharedFunctions.gridLayoutAddComponent(panelGetProduct,gc,comboID,1,6,1, 0.1);
    }

    private  void setupPanelGetDetailsInitialiseComponents() {
        panelGetProduct = new JPanel(new GridBagLayout());
        textName = new JTextField(13);
        textBrand = new JTextField(13);
        textDescription = new JTextArea(5,13);
        textPrice = new JTextField(13);
        textQuantity = new JTextField(13);
        textImgLocation = new JTextField(13);
        labelName = new JLabel("Name *");
        labelBrand = new JLabel("Brand");
        labelDescription = new JLabel("Description *");
        labelPrice = new JLabel("Price *");
        labelQuantity = new JLabel("Quantity");
        labelImgLocation = new JLabel("Img location");
        labelID = new JLabel("ID");
        box = Box.createHorizontalBox();
        box.add(new JScrollPane(textDescription));

    }

    private  void setupPanelBtns() {
        //BUTTON PANEL SETUP
        panelBtns = new JPanel(new GridLayout(3,1));
        btnCreate = new JButton("Create");
        btnDelete = new JButton("Delete");
        btnUpdate = new JButton("Update");
        SharedFunctions.buttonMouseEvents(btnCreate);
        SharedFunctions.buttonMouseEvents(btnDelete);
        SharedFunctions.buttonMouseEvents(btnUpdate);
        lb = new LineBorder(Color.WHITE);
        dim = new Dimension(160,25);

        btnCreate.setBorder(lb);
        btnCreate.setPreferredSize(dim);
        btnCreate.setBackground(Color.LIGHT_GRAY);

        btnDelete.setBorder(lb);
        btnDelete.setPreferredSize(dim);
        btnDelete.setBackground(Color.LIGHT_GRAY);

        btnUpdate.setBorder(lb);
        btnUpdate.setPreferredSize(dim);
        btnUpdate.setBackground(Color.LIGHT_GRAY);

        panelBtns.add(btnCreate);
        panelBtns.add(btnUpdate);
        panelBtns.add(btnDelete);
        panelBtns.setPreferredSize(new Dimension(200,110));
    }

    private  void setupJTable()  {
        colNames = new Vector<>();
            colNames.add("ID");
            colNames.add("NAME");
            colNames.add("BRAND");
            colNames.add("DESCRIPTION");
            colNames.add("PRICE");
            colNames.add("QUANTITY");
            colNames.add("IMAGE");
        table = DBControls.getProductTable(colNames);
        pane = new JScrollPane(table);
    }
}
