package gui.admin;

/**
 * @author  Erlandas Bacauskas
 * <br><b>Description</b> : <br>
 *      Main admin class that deals what component to draw on the window
 */

import controller.DBControls;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class AdminMain extends JFrame {

    /**
     * <br><b>Description</b> : <br>
     * Constructor that displays Administrator window
     * Does setup for admin JFrame
     * Adds toolbar/menu
     * Adds controls for components
     */
    public AdminMain() {

        setupJFrame();
        add(ToolbarAdmin.panelToolbar(),BorderLayout.NORTH);
        buttonControls();

    }

    private void buttonControls() {

        ToolbarAdmin.btnProduct.addActionListener(al -> {
            ProductPanel pp = new ProductPanel();
            setPanel(pp.panelProduct());
        });

        ToolbarAdmin.btnCustomers.addActionListener(al -> {
            CustomerPanel cp = new CustomerPanel();
            setPanel(cp.CustomerPanel());
        });

        ToolbarAdmin.btnOrder.addActionListener(al -> {
            OrdersPanel op = new OrdersPanel();
            setPanel(op.OrdersPanel());
        });
    }

    private void setPanel(JPanel panel) {
        getContentPane().removeAll();
        getContentPane().add(ToolbarAdmin.panelToolbar(),BorderLayout.NORTH);
        getContentPane().add(panel,BorderLayout.CENTER);//Adding to content pane, not to Frame
        validate();
        repaint();
        printAll(getGraphics());//Extort print all content
        buttonControls();
    }

    private void setupJFrame() {
        setLayout(new BorderLayout(0,0));
        setLocation(100, 25);
        setTitle("Administrator Panel");
        //setSize(1080,600);
        setVisible(true);
        setResizable(false);
        setMinimumSize(new Dimension(1080,600));
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing( WindowEvent e) {
                DBControls.closeConnection();
                e.getWindow().dispose();
            }
        });
    }


}
