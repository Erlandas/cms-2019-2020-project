package gui.admin;

/**
 * @author  Erlandas Bacauskas
 * Description:
 *      This class holds all the configuration for the orders window
 *      Including all the components
 */

import controller.DBControls;
import db.Customer;
import db.TableCustomer;
import gui.shared.SharedFunctions;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;

public class OrdersPanel extends JFrame {

    //Panels needed for layout orders
    private JPanel panelMain;
    private JPanel panelInvoices;
    private JPanel panelProducts;
    private JPanel panelCustomerData;

    //For panelInvoices
    private JTable tableInvoices;
    private JScrollPane scrollTableInvoices;
    private Vector<String> colNamesTableInvoice;
    private int rowSelectedTableInvoices =-1;

    //For panelProducts
    private JTable tableOfProducts;
    private JScrollPane scrollTableOfProducts;
    private Vector<String> colNamesPanelProduct;

    //For panelCustomerData
    private JLabel labelID;
    private JLabel labelCustomerID;
    private JLabel labelDetails;
    private JLabel labelCustomerName;
    private JLabel labelCustomerAddOne;
    private JLabel labelCustomerAddTwo;
    private JLabel labelCustomerTown;
    private JLabel labelCustomerCounty;
    private JLabel labelCustomerCountry;
    private JLabel labelCustomer;

    /**
     *
     * @return orders panel -   type JPanel
     * <br><b>Description</b> : <br>
     *      Constructor that does initial setup for order panel by calling private methods for :
     *      1.  Initialise components
     *      2.  Setup invoice panel
     *      3.  Setup product panel
     *      4.  Setup customer panel
     *      5.  Add controls to components
     */

    public JPanel OrdersPanel() {
        panelMain = new JPanel(new BorderLayout());
        panelMain.setBackground(Color.WHITE);
        initialiseComponents();
        setupPanelInvoices();
        setupPanelProducts();
        setupPanelCustomerData();
        controls();

        panelMain.add(panelInvoices, BorderLayout.WEST);
        panelMain.add(panelCustomerData, BorderLayout.EAST);
        panelMain.add(panelProducts, BorderLayout.SOUTH);

        return panelMain;
    }

    private void initialiseComponents() {
        labelID = new JLabel("ID : ");
        labelCustomerID = new JLabel();
        labelDetails = new JLabel("Details : ");
        labelCustomerName = new JLabel();
        labelCustomerAddOne = new JLabel();
        labelCustomerAddTwo = new JLabel();
        labelCustomerTown = new JLabel();
        labelCustomerCounty = new JLabel();
        labelCustomerCountry = new JLabel();
        labelCustomer = new JLabel("CUSTOMER : ");
    }

    private void setTextForLabels(Customer c) {
        labelCustomerID.setText(""+c.getID());
        labelCustomerName.setText(c.getFirstName()+ " " + c.getLastName());
        labelCustomerAddOne.setText(c.getAddressLine1());
        labelCustomerAddTwo.setText(c.getAddressLine2());
        labelCustomerTown.setText(c.getTown());
        labelCustomerCounty.setText(c.getCounty());
        labelCustomerCountry.setText(c.getCountry());

    }

    private void controls() {
        tableInvoices.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                rowSelectedTableInvoices = tableInvoices.getSelectedRow();
                addProductTable();
                String ID = ""+tableInvoices.getValueAt(rowSelectedTableInvoices,1);
                int id = Integer.parseInt(ID.substring(8));
                Customer customer = TableCustomer.getCustomerByID(id);
                rowSelectedTableInvoices = -1;
                setTextForLabels(customer);
            }
        });
    }

    private void addProductTable() {
        DBControls.setInvoiceSqlSyntax(""+tableInvoices.getValueAt(rowSelectedTableInvoices,1));
        colNamesPanelProduct = new Vector<>();
        colNamesPanelProduct.addElement("NAME");
        colNamesPanelProduct.addElement("BRAND");
        colNamesPanelProduct.addElement("DESCRIPTION");
        colNamesPanelProduct.addElement("PRICE");
        colNamesPanelProduct.addElement("QUANTITY");
        tableOfProducts = DBControls.getInvoiceProducts(colNamesPanelProduct);
        scrollTableOfProducts = new JScrollPane(tableOfProducts);
        SharedFunctions.refreshTable(panelProducts,scrollTableOfProducts);
    }

    private void setupPanelInvoices() {
        panelInvoices = new JPanel(new BorderLayout());
        panelInvoices.setBackground(Color.darkGray);
        panelInvoices.setPreferredSize(new Dimension(570,0));

        colNamesTableInvoice = new Vector<>();
            colNamesTableInvoice.addElement("INVOICE ID");
            colNamesTableInvoice.addElement("INVOICE TABLE");
            colNamesTableInvoice.addElement("DATE CREATED");

        tableInvoices = DBControls.getInvoicesTableReady(colNamesTableInvoice);
        scrollTableInvoices = new JScrollPane(tableInvoices);
        panelInvoices.add(scrollTableInvoices,BorderLayout.CENTER);

    }

    private void setupPanelProducts() {
        panelProducts = new JPanel(new BorderLayout());
        panelProducts.setPreferredSize(new Dimension(0,250));


    }

    private void setupPanelCustomerData() {
        panelCustomerData = new JPanel(new GridBagLayout());
        panelCustomerData.setBackground(Color.WHITE);
        panelCustomerData.setPreferredSize(new Dimension(500,300));

        layoutPanelCustomerData();

    }

    private void layoutPanelCustomerData() {
        GridBagConstraints gc = new GridBagConstraints();
        gc.fill = GridBagConstraints.NONE;

        SharedFunctions.gridLayoutAddComponent(panelCustomerData,gc,labelCustomer,0,0,1,1);
        SharedFunctions.gridLayoutAddComponent(panelCustomerData,gc,labelID,0,1,1,1);
        SharedFunctions.gridLayoutAddComponent(panelCustomerData,gc,labelCustomerID,1,1,15,1);
        SharedFunctions.gridLayoutAddComponent(panelCustomerData,gc,labelDetails,0,2,1,1);
        SharedFunctions.gridLayoutAddComponent(panelCustomerData,gc,labelCustomerName,1,2,1,1);

        SharedFunctions.gridLayoutAddComponent(panelCustomerData,gc,labelCustomerAddOne,1,3,1,1);
        SharedFunctions.gridLayoutAddComponent(panelCustomerData,gc,labelCustomerAddTwo,1,4,1,1);
        SharedFunctions.gridLayoutAddComponent(panelCustomerData,gc,labelCustomerTown,1,5,1,1);
        SharedFunctions.gridLayoutAddComponent(panelCustomerData,gc,labelCustomerCounty,1,6,1,1);
        SharedFunctions.gridLayoutAddComponent(panelCustomerData,gc,labelCustomerCountry,1,7,1,10);

    }




}
