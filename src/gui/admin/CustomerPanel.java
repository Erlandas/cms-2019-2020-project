package gui.admin;

/**
 * @author  Erlandas Bacauskas
 * Description:
 *      This class holds all the configuration for the customer window
 *      Including all the components
 */

import controller.DBControls;
import db.Customer;
import gui.shared.SharedFunctions;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
 import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Vector;

public class CustomerPanel {

    //Table
    private  JTable tableCustomer;

    //Labels
    private  JLabel labelID;
    private  JLabel labelEmail;
    private  JLabel labelName;
    private  JLabel labelSurname;
    private  JLabel labelAdd1;
    private  JLabel labelAdd2;
    private  JLabel labelTown;
    private  JLabel labelCounty;
    private  JLabel labelCountry;
    private  JLabel labelPassword;
    private  JLabel labelPhone;
    private  JLabel labelGender;
    private  JLabel labelAge;
    private  ArrayList<Customer> li;
    private  String[] colNames;
    private  Vector<String> names;
    private  JPanel panelDetailsCustomer;
    private  JScrollPane tableScrollCustomers;
    private  JPanel panelMain;
    private  GridBagConstraints gc;

    private  JLabel labelChPass;
    private  JTextField textPass;

    //buttons panel
    private  JPanel detailsMain;
    private  JPanel btnsPanel;
    private  JButton btnRem;
    private  JButton btnUpd;
    private  LineBorder lb;
    private  Dimension dim;
    private  int row;
    private  JPanel panelForTable;

    /**
     *
     * @return customer panel   -   type JPanel
     * <br><b>Description</b> : <br>
     *      Constructor that does initial setup for customer panel by calling private methods for :
     *      1.  Setup table
     *      2.  Initialise components
     *      3.  Layout components
     *      4.  Setup button panel
     *      5.  Add controls to components
     */

    public  JPanel CustomerPanel() {
        row = -1;
        colNames = new String[]{
                "FIRST NAME", "LAST NAME", "EMAIL", "ACTIVE FROM"};
        setUpTablePanel();
        initialiseComponents();
        layoutComponents();
        panelMain.add(panelForTable, BorderLayout.CENTER);
        setupButtonPanel();
        panelMain.add(detailsMain,BorderLayout.WEST);
        controls();
        return panelMain;

    }

    private  void setupButtonPanel() {
        detailsMain = new JPanel(new BorderLayout());
        detailsMain.add(panelDetailsCustomer,BorderLayout.CENTER);
        btnsPanel = new JPanel(new GridLayout(2,1));
        btnRem = new JButton("Remove Customer");
        btnUpd = new JButton("Update Password");
        btnsPanel.add(btnUpd);
        btnsPanel.add(btnRem);

        SharedFunctions.buttonMouseEvents(btnRem);
        SharedFunctions.buttonMouseEvents(btnUpd);

        lb = new LineBorder(Color.WHITE);
        dim = new Dimension(160,25);

        btnRem.setBorder(lb);
        btnRem.setPreferredSize(dim);
        btnRem.setBackground(Color.LIGHT_GRAY);
        btnUpd.setBorder(lb);
        btnUpd.setPreferredSize(dim);
        btnUpd.setBackground(Color.LIGHT_GRAY);

        btnsPanel.setPreferredSize(new Dimension(200,74));
        detailsMain.add(btnsPanel,BorderLayout.SOUTH);
    }

    private  void layoutComponents() {
        gc = new GridBagConstraints();
        gc.fill = GridBagConstraints.NONE;
        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,new JLabel("ID : "),0,0,1,1);
        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,labelID,1,0,1,1);

        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,new JLabel("Email : "),0,1,1,1);
        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,labelEmail,1,1,1,1);

        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,new JLabel("Name : "),0,2,1,1);
        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,labelName,1,2,1,1);

        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,new JLabel("Surname : "),0,3,1,1);
        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,labelSurname,1,3,1,1);

        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,new JLabel("Address 1 : "),0,4,1,1);
        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,labelAdd1,1,4,1,1);

        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,new JLabel("Address 2 : "),0,5,1,1);
        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,labelAdd2,1,5,1,1);

        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,new JLabel("Town : "),0,6,1,1);
        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,labelTown,1,6,1,1);

        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,new JLabel("County : "),0,7,1,1);
        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,labelCounty,1,7,1,1);

        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,new JLabel("Country : "),0,8,1,1);
        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,labelCountry,1,8,1,1);

        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,new JLabel("Password : "),0,9,1,1);
        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,labelPassword,1,9,1,1);

        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,new JLabel("Phone : "),0,10,1,1);
        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,labelPhone,1,10,1,1);

        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,new JLabel("Gender : "),0,11,1,1);
        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,labelGender,1,11,1,1);

        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,new JLabel("Age : "),0,12,1,15);
        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,labelAge,1,12,1,15);

        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,labelChPass,0,13,1,1);
        SharedFunctions.gridLayoutAddComponent(panelDetailsCustomer,gc,textPass,1,13,1,1);


    }

    private  void controls() {
        tableCustomer.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                row = tableCustomer.getSelectedRow();
                labelID.setText(""+li.get(row).getID());
                labelEmail.setText(li.get(row).getEmail());
                labelName.setText(li.get(row).getFirstName());
                labelSurname.setText(li.get(row).getLastName());
                labelAdd1.setText(li.get(row).getAddressLine1());
                labelAdd2.setText(li.get(row).getAddressLine2());
                labelTown.setText(li.get(row).getTown());
                labelCounty.setText(li.get(row).getCounty());
                labelCountry.setText(li.get(row).getCountry());
                labelPassword.setText(li.get(row).getPassword());
                labelPhone.setText(li.get(row).getPhone());
                labelGender.setText(li.get(row).getGender());
                labelAge.setText(""+li.get(row).getAge());
            }
        });

        btnRem.addActionListener(al -> {
            if(row == -1) {
                JOptionPane.showMessageDialog(null,"Please Select Customer");
                return;
            } else if (li.get(row).getID() == li.get(1).getID()) {
                JOptionPane.showMessageDialog(null,"Administrator Cannot Be Removed");
                return;
            }
            else if (!DBControls.removeCustomer(li.get(row).getID())) {
                JOptionPane.showMessageDialog(null,"We Couldn't Remove Customer");
                return;
            }

            setupJTable();
            SharedFunctions.refreshTable(panelForTable,tableScrollCustomers);
            ToolbarAdmin.btnCustomers.doClick();


        });

        btnUpd.addActionListener(al -> {
            if(row == -1) {
                JOptionPane.showMessageDialog(null,"Please Select Customer");
                return;
            }
            String txtPassChange = textPass.getText();
            if(txtPassChange.isEmpty()) {
                labelChPass.setForeground(Color.red);
                return;
            }else if (txtPassChange.length() < 4) {
                JOptionPane.showMessageDialog(null,"Password Must Be At Least 4 Characters Long");
                return;
            }


            if(!DBControls.updateCustomerPassword(li.get(row).getID(),txtPassChange)) {
                JOptionPane.showMessageDialog(null,"We Couldn't Update Password");
                return;
            }
            ToolbarAdmin.btnCustomers.doClick();

        });
    }

    private  void setUpTablePanel() {
        panelForTable = new JPanel(new BorderLayout());
        setupJTable();
        panelForTable.add(tableScrollCustomers);
        tableScrollCustomers.setBackground(Color.WHITE);
        panelForTable.add(tableScrollCustomers);
    }

    private  void setupJTable() {
        names = new Vector<>();
        for(String name: colNames){
            names.add(name);
        }
        tableCustomer = DBControls.getCustomerTable(names);
        tableScrollCustomers = new JScrollPane(tableCustomer);
    }

    private  void initialiseComponents() {

        panelDetailsCustomer = new JPanel(new GridBagLayout());
        panelDetailsCustomer.setPreferredSize(new Dimension(250,600));
        panelMain = new JPanel(new BorderLayout());
        labelID = new JLabel();
        labelEmail = new JLabel();
        labelName = new JLabel();
        labelSurname = new JLabel();
        labelAdd1 = new JLabel();
        labelAdd2 = new JLabel();
        labelTown = new JLabel();
        labelCounty = new JLabel();
        labelCountry = new JLabel();
        labelPassword = new JLabel();
        labelPhone = new JLabel();
        labelGender = new JLabel();
        labelAge = new JLabel();
        labelChPass = new JLabel("New Password : ");
        textPass = new JTextField(11);
        li = DBControls.getCustomers();
    }

}
