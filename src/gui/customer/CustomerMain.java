package gui.customer;

/**
 * @author  Erlandas Bacauskas
 * Description:
 *      Class that deals with main customer panel
 *      Adds all the components as needed depends
 *      On the toolbar option
 *
 */

import controller.DBControls;
import db.Customer;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;

public class CustomerMain extends JFrame{

    private static Customer c;
    protected static String basketName;
    private String invoiceName;

    /**
     *
     * @param email -   type string
     * <br><b>Description</b> : <br>
     *              Displays main customer window
     *              Constructor that takes user email as argument and retrieves
     *              customer type object from database using passed email
     *              Creates invoice table for customer to store items after committing purchase
     *              Initialises SQL syntax for newly created invoice table
     *              Creates basket table as temporary holding place for products before committing purchase
     *              Initialises SQl syntax for newly created basket table <br>
     * uses following private functions :
     *              1.  Setup JFrame
     *              2.  Add controls to components
     */

    public CustomerMain(String email) {
        invoiceName = "invoice_";
        DBControls.establishConnection();
        //Gets Customer that is logged in OBJECT
        c = DBControls.getActiveCustomer(email);
        //Sets invoice syntax for SQL
        DBControls.setInvoiceSqlSyntax(invoiceName + c.getID());
        resetTable();
        setupJFrame();
        add(ToolbarCustomer.panelToolbar(),BorderLayout.NORTH);
        buttonControls();

    }

    /**
     * <br><b>Description</b> : <br>
     * Method that initialises Basket table for user as temporary holding place
     * for items before committing purchase.
     * Also sets SQL syntax for newly created basket table in Database
     */

    static void resetTable() {
        basketName = "Basket_" + c.getID();
        DBControls.setSqlBasketCreateSqlSyntax(basketName);
        DBControls.basketCreate();
    }

    private void buttonControls() {
        ToolbarCustomer.btnMain.addActionListener(al -> {
            PurchasePanel purchasePanel = new PurchasePanel();
            try {
                setPanel(purchasePanel.PurchasePanel(c));
            } catch (SQLException e) {
                e.printStackTrace();
            }

        });

        ToolbarCustomer.btnHistory.addActionListener(al -> {
            HistoryPanel pendingPanel = new HistoryPanel();
            setPanel(pendingPanel.PendingPanel());
        });

    }

    private void setPanel(JPanel panel) {
        getContentPane().removeAll();
        getContentPane().add(ToolbarCustomer.panelToolbar(),BorderLayout.NORTH);
        getContentPane().add(panel,BorderLayout.CENTER);//Adding to content pane, not to Frame
        validate();
        repaint();
        printAll(getGraphics());//Extort print all content
        buttonControls();
    }

    private void setupJFrame() {
        setLayout(new BorderLayout(0,0));
        setLocation(100, 25);
        setTitle(c.getFirstName() + " " + c.getLastName());
        //setSize(1080,600);
        setVisible(true);
        setResizable(false);
        setMinimumSize(new Dimension(1080,600));
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing( WindowEvent e) {
                DBControls.closeConnection();
                e.getWindow().dispose();
            }
        });
    }
}
