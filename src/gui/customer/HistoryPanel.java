package gui.customer;

/**
 * @author  Erlandas Bacauskas
 * <br><b>Description</b> : <br>
 *      Class that displays table of purchase history
 */

import controller.DBControls;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

public class HistoryPanel {

    private static JPanel mainPanel;
    private static JTable tablePending;
    private static JScrollPane scrollTablePending;
    private Vector<String> colNames;

    /**
     * Constructor that creates JPanel from items in the invoice table
     * @return history panel -  type JPanel
     */
    public JPanel PendingPanel() {

        colNames = new Vector<>();
            colNames.add("NAME");
            colNames.add("BRAND");
            colNames.add("PRICE");
            colNames.add("DESCRIPTION");
            colNames.add("QUANTITY");
        mainPanel = new JPanel(new GridLayout());
        tablePending = DBControls.getInvoiceProducts(colNames);
        scrollTablePending = new JScrollPane(tablePending);
        mainPanel.add(scrollTablePending);

        return mainPanel;
    }

}
