package gui.customer;

/**
 * @author  Erlandas Bacauskas
 * <br><b>Description</b> : <br>
 *      Class that creates Toolbar with an options
 */

import gui.shared.SharedFunctions;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class ToolbarCustomer {
    private static JPanel panelToolbar;
    protected static JButton btnMain;
    protected static JButton btnHistory;

    private static LineBorder lb;
    private static Dimension dim;

    /**
     * @return type JPanel
     * <br><b>Description</b> : <br>
     *      Does initial setup of top toolbar with a buttons that navigates through panels
     */

    public static JPanel panelToolbar() {

        panelToolbar = new JPanel();
        lb = new LineBorder(Color.WHITE);
        dim = new Dimension(160,25);

        btnMain = new JButton("Main");
        btnHistory = new JButton("History");


        btnMain.setBorder(lb);
        btnMain.setPreferredSize(dim);
        btnMain.setBackground(Color.LIGHT_GRAY);

        btnHistory.setBorder(lb);
        btnHistory.setPreferredSize(dim);
        btnHistory.setBackground(Color.LIGHT_GRAY);

        SharedFunctions.buttonMouseEvents(btnMain);
        SharedFunctions.buttonMouseEvents(btnHistory);

        panelToolbar.setLayout(new FlowLayout(FlowLayout.RIGHT));

        panelToolbar.add(btnMain);
        panelToolbar.add(btnHistory);
        panelToolbar.setBackground(Color.LIGHT_GRAY);
        return panelToolbar;

    }
}
