package gui.customer;

/**
 * @author  Erlandas Bacauskas
 * <br><b>Description</b> : <br>
 *      Class that creates purchases panel in customer window
 *      All method names are relevant to operations it does
 *      So no comments provided as all functions has basic capabilities
 */

import controller.DBControls;
import db.Customer;
import db.Product;
import db.TableInvoice;
import gui.shared.SharedFunctions;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

public class PurchasePanel {

    private JPanel panelMain;
    private JPanel panelSelectProductMain;
    private JPanel panelSelectProductDetails;
    private JPanel panelSelectProductPhoto;
    private JPanel panelSelectProductButtons;
    private JPanel panelTable;

    //for select product details
    private JLabel labelItemName;
    private JComboBox comboProductNames;
    private JLabel labelAmount;
    private JTextField textAmount;
    private JLabel labelBrand;
    private JLabel labelProductBrand;
    private JLabel labelQuantity;
    private JLabel labelProductQuantity;
    private JLabel labelDescription;
    private JTextArea areaDescription;
    private Box boxDescription;
    private JLabel labelPrice;
    private JLabel labelProductPrice;
    private GridBagConstraints gc;
    private ArrayList<Product> listProduct;

    //for buttons panel
    private JButton btnAddToBasket;
    private JButton btnRemoveFromBasket;
    private JButton btnPurchase;
    private JButton btnClearBasket;
    private LineBorder borderBtn;
    private Dimension dimBtn;

    //Table
    private static JTable tableOfProducts;
    private int index;
    private int rowSelected;
    private Vector<String> colNames;
    private static Product pr;

    private Customer activeCustomer;
    private String invoicePrefix;

    /**
     *
     * @param c -   type Customer
     * @return  -   type JPanel
     * @throws SQLException -   could throw SQLException
     * <br><b>Description</b> : <br>
     *      Initialises JPanel as purchase panel using following private functions :
     *      1.  Initialise components
     *      2.  Setup table
     *      3.  Setup panel
     *      4.  Adds controls for the components
     */

    public JPanel PurchasePanel(Customer c) throws SQLException {
        this.activeCustomer = c;

        panelMain = new JPanel(new BorderLayout());

        initialiseComponents();
        setUpPanelTable();
        setUpPanelSelectProductMain();
        controls();

        panelMain.add(panelSelectProductMain,BorderLayout.WEST);
        panelMain.add(panelTable,BorderLayout.CENTER);
        return panelMain;
    }

    private void setUpPanelTable() {
        panelTable = new JPanel(new GridLayout());
        colNames = new Vector<>();
            colNames.add("ID");
            colNames.add("PRODUCT ID");
            colNames.add("NAME");
            colNames.add("BRAND");
            colNames.add("PRICE");
            colNames.add("QUANTITY");
        tableOfProducts = DBControls.getBasketTableReady(colNames);
        panelTable.add(new JScrollPane(tableOfProducts));
    }

    private void initialiseComponents() {
        invoicePrefix = "invoice_";
        //Table
        colNames = new Vector<>();
        tableOfProducts = new JTable();
        index = -1;
        rowSelected = -1;
        //Details panel
        labelItemName = new JLabel("Product : ");
        listProduct = DBControls.getProducts();
        setupComboProducts();
        labelAmount = new JLabel("Amount : ");
        textAmount = new JTextField(10);
        labelBrand = new JLabel("Brand : ");
        labelProductBrand = new JLabel();
        labelQuantity = new JLabel("Quantity : ");
        labelProductQuantity = new JLabel();
        labelDescription = new JLabel("Description : ");
        areaDescription = new JTextArea(5,10);
        boxDescription = Box.createHorizontalBox();
        boxDescription.setBorder(new LineBorder(Color.DARK_GRAY));
        areaDescription.setEditable(false);
        areaDescription.setWrapStyleWord(true);
        boxDescription.add(areaDescription);
        labelPrice = new JLabel("Price : ");
        labelProductPrice = new JLabel();
        gc = new GridBagConstraints();

        //Buttons
        btnAddToBasket = new JButton("Add To Basket");
        btnRemoveFromBasket = new JButton("Remove From Basket");
        btnPurchase = new JButton("Purchase");
        btnClearBasket = new JButton("Clear Basket");
        borderBtn = new LineBorder(Color.WHITE);
        dimBtn = new Dimension(160,33);
    }

    private void setupComboProducts() {
        comboProductNames = new JComboBox();
        DefaultComboBoxModel comboProductNameModel = new DefaultComboBoxModel();
        comboProductNameModel.addElement("");
        for(int i = 0; i < listProduct.size() ; i++) {
            Product pr = listProduct.get(i);
            comboProductNameModel.addElement(pr.getName());
        }
        comboProductNames.setModel(comboProductNameModel);
        comboProductNameModel.setSelectedItem("");
    }

    private void setUpPanelSelectProductMain() {
        panelSelectProductMain =new JPanel(new BorderLayout());
        panelSelectProductMain.setBackground(Color.WHITE);
        panelSelectProductMain.setPreferredSize(new Dimension(500,200));

        setUpPanelSelectProductDetails();
        setUpPanelSelectProductPhoto();
        setUpPanelSelectProductButtons();

        panelSelectProductMain.add(panelSelectProductPhoto,BorderLayout.CENTER);
        panelSelectProductMain.add(panelSelectProductDetails,BorderLayout.NORTH);
        panelSelectProductMain.add(panelSelectProductButtons,BorderLayout.SOUTH);
    }

    private void controls() {

        if(tableOfProducts != null) {
            //Selected row recorded to variable
            tableOfProducts.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    rowSelected = tableOfProducts.getSelectedRow();
                }
            });
        }
        //Fills in details for product from selected combo item
        comboProductNames.addActionListener(e -> {
            index = comboProductNames.getSelectedIndex()-1;
            if(index == -1) {
                setFieldText("","","","");
                return;
            }
            pr = listProduct.get(index);
            setFieldText(pr.getBrand(),""+pr.getQuantity(),pr.getDescription(),""+pr.getPrice());
        });

        //Adds product to a table
        btnAddToBasket.addActionListener(e -> {

            if(index == -1) return;
            else if(tableOfProducts == null) {
                CustomerMain.resetTable();
            }
            else if(textAmount.getText().isEmpty()) {
                labelAmount.setForeground(Color.red);
                return;
            }

            try {
                Integer.parseInt(textAmount.getText());
            } catch (Exception ex) {
                labelAmount.setForeground(Color.red);
                return;
            }

            if(Integer.parseInt(textAmount.getText()) <= 0) {
                labelAmount.setForeground(Color.red);
                return;
            }
            else if(Integer.parseInt(labelProductQuantity.getText()) < Integer.parseInt(textAmount.getText())) {
                labelQuantity.setForeground(Color.red);
                return;
            }

            pr = listProduct.get(index);
            DBControls.basketAddTo(pr,textAmount.getText());
            ToolbarCustomer.btnMain.doClick();
        });

        //Removes product from a table
        btnRemoveFromBasket.addActionListener(e -> {
            if(rowSelected == -1) return;
            DBControls.basketRemoveItem((int)(tableOfProducts.getValueAt(rowSelected,0)));
            ToolbarCustomer.btnMain.doClick();
            rowSelected = -1;
        });

        btnPurchase.addActionListener(e -> {
            DBControls.basketDeleteTable();
            TableInvoice.invoiceCreate();
            if(tableOfProducts == null) {
                return;
            }
            if(tableOfProducts.getRowCount() == 0) return;
            for( int i = 0 ; i < tableOfProducts.getRowCount() ; i++ ) {
                TableInvoice.invoiceAddItem(
                        (int)tableOfProducts.getValueAt(i,1),
                        activeCustomer.getID(),
                        ""+tableOfProducts.getValueAt(i,5));
            }

            DBControls.invoiceAddRecord(invoicePrefix + activeCustomer.getID());
            tableOfProducts = null;
            ToolbarCustomer.btnMain.doClick();
        });

        //Resets table to empty cleans all the items in it
        btnClearBasket.addActionListener(e -> {
            DBControls.basketDeleteTable();
            CustomerMain.resetTable();
            ToolbarCustomer.btnMain.doClick();
        });

    }

    private void setFieldText(String brand, String quantity, String description, String price) {
        labelProductBrand.setText(brand);
        labelProductPrice.setText(price);
        labelProductQuantity.setText(quantity);
        areaDescription.setText(description);
    }

    private void setUpPanelSelectProductButtons() {
        panelSelectProductButtons = new JPanel(new GridLayout(2,2));

        setUpButton(btnAddToBasket);
        setUpButton(btnRemoveFromBasket);
        setUpButton(btnPurchase);
        setUpButton(btnClearBasket);

        SharedFunctions.buttonMouseEvents(btnAddToBasket);
        SharedFunctions.buttonMouseEvents(btnRemoveFromBasket);
        SharedFunctions.buttonMouseEvents(btnPurchase);
        SharedFunctions.buttonMouseEvents(btnClearBasket);

        panelSelectProductButtons.add(btnAddToBasket);
        panelSelectProductButtons.add(btnPurchase);
        panelSelectProductButtons.add(btnRemoveFromBasket);
        panelSelectProductButtons.add(btnClearBasket);
    }

    private void setUpButton(JButton b) {
        b.setBorder(borderBtn);
        b.setPreferredSize(dimBtn);
        b.setBackground(Color.LIGHT_GRAY);
    }

    private void setUpPanelSelectProductDetails() {
        panelSelectProductDetails = new JPanel(new GridBagLayout());
        panelSelectProductDetails.setBackground(Color.lightGray);
        panelSelectProductDetails.setPreferredSize(new Dimension(0,150));
        layoutPanelSelectProductDetails();
    }

    private void layoutPanelSelectProductDetails() {
        gc.fill = GridBagConstraints.NONE;

        int y = 0;
        SharedFunctions.gridLayoutAddComponent(panelSelectProductDetails,gc,labelItemName,0,y,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSelectProductDetails,gc,comboProductNames,1,y,1, 1);
        SharedFunctions.gridLayoutAddComponent(panelSelectProductDetails,gc,labelAmount,2,y,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSelectProductDetails,gc,textAmount,3,y,1, 1);

        y++;
        SharedFunctions.gridLayoutAddComponent(panelSelectProductDetails,gc,labelBrand,0,y,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSelectProductDetails,gc,labelProductBrand,1,y,1, 1);
        SharedFunctions.gridLayoutAddComponent(panelSelectProductDetails,gc,labelQuantity,2,y,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSelectProductDetails,gc,labelProductQuantity,3,y,1, 1);

        y++;
        SharedFunctions.gridLayoutAddComponent(panelSelectProductDetails,gc,labelDescription,0,y,1,5);
        SharedFunctions.gridLayoutAddComponent(panelSelectProductDetails,gc,boxDescription,1,y,1, 5);
        SharedFunctions.gridLayoutAddComponent(panelSelectProductDetails,gc,labelPrice,2,y,1,5);
        SharedFunctions.gridLayoutAddComponent(panelSelectProductDetails,gc,labelProductPrice,3,y,1, 5);


    }

    private void setUpPanelSelectProductPhoto() {
        panelSelectProductPhoto = new JPanel();
        panelSelectProductPhoto.setBackground(Color.WHITE);
        panelSelectProductPhoto.setPreferredSize(new Dimension(0,200));
        //panelSelectProductPhoto.setBorder(new LineBorder(Color.black));
    }
}
