package gui.loginSignUp;
/**
 * @author  Erlandas Bacauskas
 * Description:
 *      Sign up form that takes input from fields and creates users
 *      Also as does exception check
 */
import controller.DBControls;
import gui.TestDriver;
import gui.shared.SharedFunctions;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class SignUpForm extends JFrame  {

    private String[] countiesIreland = {
            "Co. Antrim",
            "Co. Armagh",
            "Co. Carlow",
            "Co. Cavan",
            "Co. Clare",
            "Co. Cork",
            "Co. Derry",
            "Co. Donegal",
            "Co. Down",
            "Co. Dublin",
            "Co. Fermanagh",
            "Co. Galway",
            "Co. Kerry",
            "Co. Kildare",
            "Co. Kilkenny",
            "Co. Laois",
            "Co. Leitrim",
            "Co. Limerick",
            "Co. Longford",
            "Co. Louth",
            "Co. Mayo",
            "Co. Meath",
            "Co. Monaghan",
            "Co. Offaly",
            "Co. Roscommon",
            "Co. Sligo",
            "Co. Tipperary",
            "Co. Tyrone",
            "Co. Waterford",
            "Co. Westmeath",
            "Co. Wexford",
            "Co. Wicklow",
            "Other"
    };

    private String[] countrys = {"IRL","USA","UK"};

    //Needed panels also as extra components
    private JPanel panelSignUpCenter;
    private JPanel panelImgNorth;
    private JPanel panelBtnSouth;
    private GridBagConstraints gc;

    //Needed images
    private ImageIcon imgSignUp;
    private Image imgSignUpResized;
    private String imgDestination;

    //--    Needed labels for SIGN UP form
    private JLabel labelFirstName;
    private JLabel labelLastName;
    private JLabel labelEmail;
    private JLabel labelEmailConfirm;
    private JLabel labelAddressL1;
    private JLabel labelAddressL2;
    private JLabel labelTown;
    private JLabel labelCounty;
    private JLabel labelCountry;
    private JLabel labelPhone;
    private JLabel labelGender;
    private JLabel labelAge;
    private JLabel labelPassword;
    private JLabel labelPasswordConfirm;
    private JLabel labelImgSignUp;
    private JLabel labelTermsConditions;

    //--    Needed text fields for data extra size component
    private JTextField textFirstName;
    private JTextField textLastName;
    private JTextField textEmail;
    private JTextField textEmailConfirm;
    private JTextField textAddressL1;
    private JTextField textAddressL2;
    private JTextField textTown;
    private JTextField textPhone;
    private int size;

    //--    Needed password fields
    private JPasswordField passPassword;
    private JPasswordField passPasswordConfirm;

    //--    needed DropDown menus and extra components
    private JComboBox comboCounty;
    private JComboBox comboAge;
    private JComboBox comboCountry;
    private DefaultComboBoxModel comboCountyModel;
    private DefaultComboBoxModel comboCountryModel;
    private DefaultComboBoxModel comboAgeModel;
    private Dimension dimComboBox;

    //--    Needed radio buttons
    private JRadioButton radioMale;
    private JRadioButton radioFemale;
    private JRadioButton radioOther;
    private ButtonGroup groupGender;

    //--    Needed buttons also as extras for the buttons
    private JButton btnSignUp;
    private JButton btnCancel;
    private Dimension btnDimension;
    private LineBorder borderWhite;
    private JButton btnTermsConditions;
    private JButton btnClear;

    //--    Needed checkbox
    private JCheckBox checkTermsConditions;


    /**
     * <br><b>Description</b> : <br>
     * Constructor that does initial setup of JFrame while using following private functions for :
     *      1.  Setup JFrame
     *      2.  Setup panel location north
     *      3.  Setup panel location south
     *      4.  Setup panel location center
     *      5.  Add controls to components used
     */
    protected SignUpForm() {
        borderWhite = new LineBorder(Color.WHITE);
        setUpJFrame();
        setUpPanelImgNorth();
        setUpPanelBtnSouth();
        setUpPanelSignUpCenter();
        buttonControls();
        add(panelImgNorth, BorderLayout.NORTH);
        add(panelBtnSouth, BorderLayout.SOUTH);
        add(panelSignUpCenter, BorderLayout.CENTER);
    }

    private void setUpPanelSignUpCenter() {
        initialiseComponentsPanelSignUpCenter();
        layoutComponentsPanelSignUpCenter();
    }

    private void initialiseComponentsPanelSignUpCenter() {

        //Initialise labels with values for text
        labelFirstName = new JLabel("Name *");
        labelLastName = new JLabel("Surname *");
        labelEmail = new JLabel("Email *");
        labelEmailConfirm = new JLabel("Confirm Email *");
        labelAddressL1 = new JLabel("Address Line 1 *");
        labelAddressL2 = new JLabel("Address Line 2");
        labelTown = new JLabel("Town *");
        labelCounty = new JLabel("County *");
        labelCountry = new JLabel("Country *");
        labelPhone = new JLabel("Phone");
        labelGender = new JLabel("Gender");
        labelAge = new JLabel("Age *");
        labelPassword = new JLabel("Password *");
        labelPasswordConfirm = new JLabel("Confirm Password *");
        labelTermsConditions = new JLabel("Agree With T&C");

        //Initialise check box
        checkTermsConditions = new JCheckBox();

        //Initialise button
        btnTermsConditions = new JButton(" Read T&C");
        btnTermsConditions.setBorder(borderWhite);
        btnTermsConditions.setBackground(Color.WHITE);
        btnTermsConditions.setFont(new Font("Serif", Font.ITALIC,13));
        btnTermsConditions.setForeground(Color.BLUE);

        //initialise text fields with value for size
        size = 10;
        textFirstName = new JTextField(size);
        textLastName = new JTextField(size);
        textEmail = new JTextField(size);
        textEmailConfirm = new JTextField(size);
        textAddressL1 = new JTextField(size);
        textAddressL2 = new JTextField(size);
        textTown = new JTextField(size);
        textPhone = new JTextField(size);

        //Initialise password fields
        passPassword = new JPasswordField(size);
        passPasswordConfirm = new JPasswordField(size);

        //Radio buttons setup
        radioMale = new JRadioButton("Male");
        radioFemale = new JRadioButton("Female");
        radioOther = new JRadioButton("Other");
        groupGender = new ButtonGroup();
        radioMale.setBackground(Color.WHITE);
        radioFemale.setBackground(Color.WHITE);
        radioOther.setBackground(Color.WHITE);
        groupGender.add(radioMale);
        groupGender.add(radioFemale);
        groupGender.add(radioOther);
        radioMale.setSelected(true);
        //--    Action command sets string that we can retrieve from radio buttons
        radioMale.setActionCommand("M");
        radioFemale.setActionCommand("F");
        radioOther.setActionCommand("O");

        //SETUP combo box for counties
        dimComboBox = new Dimension(103,20);
        comboCounty = new JComboBox();
        comboCountyModel = new DefaultComboBoxModel();
        for (String county: countiesIreland) {
            comboCountyModel.addElement(county);
        }
        comboCounty.setModel(comboCountyModel);
        comboCounty.setPreferredSize(dimComboBox);
        comboCounty.setBackground(Color.WHITE);
        comboCounty.setSelectedIndex(countiesIreland.length-1);

        //SETUP combo box for countries
        comboCountry = new JComboBox();
        comboCountryModel = new DefaultComboBoxModel();
        for(String country: countrys) {
            comboCountryModel.addElement(country);
        }
        comboCountry.setModel(comboCountryModel);
        comboCountry.setPreferredSize(dimComboBox);
        comboCountry.setBackground(Color.WHITE);
        comboCountry.setSelectedIndex(0);

        //SETUP combo box for age 1 - 110
        comboAge = new JComboBox();
        comboAgeModel = new DefaultComboBoxModel();
        for (int i = 1; i <= 110; i++) {
            comboAgeModel.addElement(i);
        }
        comboAge.setModel(comboAgeModel);
        comboAge.setBackground(Color.WHITE);
        comboAge.setPreferredSize(dimComboBox);

    }

    private void layoutComponentsPanelSignUpCenter() {
        //SETUP PANEL
        //--    Initialise panel
        //--    Set background to white
        //--    Initialise GridBagConstraints for creating layout
        panelSignUpCenter = new JPanel(new GridBagLayout());
        panelSignUpCenter.setBackground(Color.WHITE);
        gc = new GridBagConstraints();
        gc.fill = GridBagConstraints.NONE;

        int gridY = 0;
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,labelFirstName,0,gridY,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,textFirstName,1,gridY,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,labelLastName,2,gridY,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,textLastName,3,gridY,1,1);

        gridY++;
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,labelEmail,0,gridY,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,textEmail,1,gridY,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,labelEmailConfirm,2,gridY,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,textEmailConfirm,3,gridY,1,1);

        gridY++;
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,labelAddressL1,0,gridY,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,textAddressL1,1,gridY,1,1);

        gridY++;
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,labelAddressL2,0,gridY,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,textAddressL2,1,gridY,1,1);

        gridY++;
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,labelTown,0,gridY,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,textTown,1,gridY,1,1);

        gridY++;
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,labelCounty,0,gridY,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,comboCounty,1,gridY,1,1);

        gridY++;
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,labelCountry,0,gridY,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,comboCountry,1,gridY,1,1);

        gridY++;
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,labelGender,0,gridY,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,radioMale,1,gridY,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,labelAge,2,gridY,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,comboAge,3,gridY,1,1);

        gridY++;
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,radioFemale,1,gridY,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,labelPhone,2,gridY,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,textPhone,3,gridY,1,1);

        gridY++;
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,radioOther,1,gridY,1,1);

        gridY++;
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,labelPassword,0,gridY,1,100);
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,passPassword,1,gridY,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,labelPasswordConfirm,2,gridY,1,1);
        SharedFunctions.gridLayoutAddComponent(panelSignUpCenter,gc,passPasswordConfirm,3,gridY,1,1);

        //----  Last Line needs some manual positioning

        gc.insets = new Insets(0,0,0,0);
        gc.gridy++;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.FIRST_LINE_END;
        panelSignUpCenter.add(checkTermsConditions,gc);
        gc.gridx++;
        gc.anchor = GridBagConstraints.FIRST_LINE_END;
        panelSignUpCenter.add(labelTermsConditions,gc);
        gc.gridx++;
        gc.anchor = GridBagConstraints.FIRST_LINE_START;
        panelSignUpCenter.add(btnTermsConditions,gc);

    }

    //Main controls for sign up form
    private void buttonControls() {

        //If selected country is not Ireland
        //disable county selection
        comboCountry.addActionListener(e -> {
            int countryIndex = comboCountry.getSelectedIndex();
            if(countryIndex != 0) {
                comboCounty.setSelectedIndex(countiesIreland.length-1);
                comboCounty.setEnabled(false);
            } else {
                comboCounty.setEnabled(true);
            }

        });

        checkTermsConditions.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                checkTermsConditions.setCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                checkTermsConditions.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
        });

        SharedFunctions.buttonMouseEvents(btnCancel);
        SharedFunctions.buttonMouseEvents(btnSignUp);
        SharedFunctions.buttonMouseEvents(btnClear);

        btnTermsConditions.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                btnTermsConditions.setCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                btnTermsConditions.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
        });

        btnCancel.setMnemonic(KeyEvent.VK_C);
        btnSignUp.setMnemonic(KeyEvent.VK_S);
        btnClear.setMnemonic(KeyEvent.VK_D);

        btnTermsConditions.addActionListener(al -> {
            TermsConditions tc = new TermsConditions();
        });

        btnClear.addActionListener(al -> {
            textFirstName.setText("");
            textLastName.setText("");
            textEmail.setText("");
            textEmailConfirm.setText("");
            textAddressL1.setText("");
            textAddressL2.setText("");
            textTown.setText("");
            textPhone.setText("");
            passPassword.setText("");
            passPasswordConfirm.setText("");
            comboCounty.setSelectedIndex(countiesIreland.length-1);
            comboCountry.setSelectedIndex(0);
            comboAge.setSelectedIndex(0);
            radioMale.setSelected(true);
        });

        btnCancel.addActionListener(al -> {
            TestDriver.start();
            DBControls.closeConnection();
            dispose();
        });

        btnSignUp.addActionListener(al -> {

            String name = textFirstName.getText();
            String surname = textLastName.getText();
            String email = textEmail.getText();
            String emailConfirm = textEmailConfirm.getText();
            String addLine1 = textAddressL1.getText();
            String addLine2 = textAddressL2.getText().isEmpty()? "":textAddressL2.getText();
            String town = textTown.getText();
            String county = (String)comboCounty.getSelectedItem();
            String country = (String)comboCountry.getSelectedItem();
            String gender = groupGender.getSelection().getActionCommand();
            int age = (int)comboAge.getSelectedItem();
            String phone = textPhone.getText().isEmpty()? "":textPhone.getText();
            String password = passPassword.getText();
            String passwordConfirm = passPasswordConfirm.getText();
            boolean isFormCompleted = true;

            //Check if email match
            if(!email.equals(emailConfirm)) {
                JOptionPane.showMessageDialog(null,
                        "Email Must Match",
                        "Email Confirmation",
                        JOptionPane.WARNING_MESSAGE);
                labelEmailConfirm.setForeground(Color.red);
                textEmailConfirm.setText("");
                return;
            }

            isFormCompleted = isFieldNotEmpty(name,labelFirstName);
            if(!isFormCompleted) return;
            isFormCompleted = isFieldNotEmpty(surname,labelLastName);
            if(!isFormCompleted) return;
            isFormCompleted = isFieldNotEmpty(email,labelEmail);
            if(!isFormCompleted) return;
            isFormCompleted = isFieldNotEmpty(emailConfirm,labelEmailConfirm);
            if(!isFormCompleted) return;
            isFormCompleted = isFieldNotEmpty(addLine1,labelAddressL1);
            if(!isFormCompleted) return;
            isFormCompleted = isFieldNotEmpty(town,labelTown);
            if(!isFormCompleted) return;

            //BASIC check if integer provided for phone number
            try  {
                long phoneNumber;
                if(!phone.isEmpty()) phoneNumber = Integer.parseInt(phone);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null,
                        "Please Provide a Number\nConcisting Only Digits",
                        "Phone Not Valid",
                        JOptionPane.WARNING_MESSAGE);
                return;
            }

            if(!DBControls.isEmailValid(email)) {
                textEmail.setText("");
                textEmailConfirm.setText("");
                return;
            };

            //Password check
            //--    If it is at least 4 characters long
            //      prevents empty password fields
            //--    Else if passwords is match
            if (password.length()<4) {
                JOptionPane.showMessageDialog(null,
                        "Minimum Length Of Password 4 Characters",
                        "Password Length",
                        JOptionPane.WARNING_MESSAGE);
                labelPassword.setForeground(Color.red);
                labelPasswordConfirm.setForeground(Color.red);
                passPassword.setText("");
                passPasswordConfirm.setText("");
                return;
            }
            else if(!password.equals(passwordConfirm)) {
                JOptionPane.showMessageDialog(null,
                        "Password Must Match",
                        "Password Confirmation",
                        JOptionPane.WARNING_MESSAGE);
                labelPassword.setForeground(Color.red);
                labelPasswordConfirm.setForeground(Color.red);
                passPassword.setText("");
                passPasswordConfirm.setText("");
                return;
            }


            isFormCompleted = isFieldNotEmpty(password,labelPassword);
            if(!isFormCompleted) return;
            isFormCompleted = isFieldNotEmpty(passwordConfirm,labelPasswordConfirm);
            if(!isFormCompleted) return;

            if(checkTermsConditions.isSelected()) {
                if(!DBControls.createCustomer(email,name,surname,addLine1,addLine2,town,county,country,password,phone,gender,age)) {
                    JOptionPane.showMessageDialog(null,
                            "Sorry we couldn't create user account at this moment\nPlease try again later",
                            "Warning",
                            JOptionPane.WARNING_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null,
                            "Customer Account Created\nplease login at the Login Screen",
                            "Account Created",
                            JOptionPane.PLAIN_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null,
                        "You Have To Agree With terms & Condition\nBefore Creating an Account",
                        "T&C",
                        JOptionPane.WARNING_MESSAGE);
                return;
            }
            TestDriver.start();
            dispose();

        });
    }

    //Checks if required fields are provided with data
    private boolean isFieldNotEmpty(String fieldName, JLabel fieldLabel){
        if(fieldName.isEmpty()) {
            fieldLabel.setForeground(Color.red);
            return false;
        }
        return true;
    }

    private void setUpJFrame() {
        setLayout(new BorderLayout(0,0));
        setLocation(350, 25);
        setTitle("Sign Up Form");
        setSize(525,600);
        setVisible(true);
        setResizable(false);
        //Default close operation also closes connection to DB
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing( WindowEvent e) {
                DBControls.closeConnection();
                e.getWindow().dispose();
            }
        });
    }

    private void setUpPanelBtnSouth() {
        //BUTTON CONFIGURATION
        //--    Create dimension for buttons
        //--    Initialise buttons
        //--    Set new borders for buttons
        //--    Change size of a buttons using dimension
        //--    Set background color to LIGHT_GRAY
        btnDimension = new Dimension(160,25);
        btnSignUp = new JButton("Sign Up");
        btnCancel = new JButton("Cancel");
        btnClear = new JButton("Clear");
        btnCancel.setBorder(borderWhite);
        btnSignUp.setBorder(borderWhite);
        btnClear.setBorder(borderWhite);
        btnSignUp.setPreferredSize(btnDimension);
        btnCancel.setPreferredSize(btnDimension);
        btnClear.setPreferredSize(btnDimension);
        btnCancel.setBackground(Color.LIGHT_GRAY);
        btnClear.setBackground(Color.LIGHT_GRAY);
        btnSignUp.setBackground(Color.LIGHT_GRAY);

        //PANEL CONFIGURATION
        //--    Initialise panel
        //--    Set background to light grey
        //--    Add the buttons
        //--    Set size of a panel
        panelBtnSouth = new JPanel();
        panelBtnSouth.setBackground(Color.LIGHT_GRAY);
        panelBtnSouth.add(btnCancel);
        panelBtnSouth.add(btnClear);
        panelBtnSouth.add(btnSignUp);
        panelBtnSouth.setSize(400,50);
    }

    private void setUpPanelImgNorth() {
        imgDestination = "src\\images\\sign_up.jpg";
        //IMAGE MANIPULATION
        //--    First Get image from the destination
        //--    Then resize image using AWT IMAGE
        //--    Assign resized image to the first image
        imgSignUp = new ImageIcon(imgDestination);
        imgSignUpResized = imgSignUp.getImage().getScaledInstance(525,150,Image.SCALE_SMOOTH);
        imgSignUp = new ImageIcon(imgSignUpResized);

        //DO basic settings for the panel
        //--    Create label with image
        //--    initialise panel
        //--    Add label
        //--    Set background
        //--    Set size
        labelImgSignUp = new JLabel(imgSignUp);
        panelImgNorth = new JPanel();
        panelImgNorth.setBackground(Color.white);
        panelImgNorth.add(labelImgSignUp);
        panelImgNorth.setSize(525,150);


    }

































//    private JPanel createPanel() {
//        //panelSignUp = new JPanel();
//
//        //Radio buttons setup
//        radioMale = new JRadioButton("Male");
//        radioFemale = new JRadioButton("Female");
//        radioOther = new JRadioButton("Other");
//        groupGender = new ButtonGroup();
//        groupGender.add(radioMale);
//        groupGender.add(radioFemale);
//        groupGender.add(radioOther);
//        radioMale.setSelected(true);
//
//        //Action command sets string that we can retrieve
//        radioMale.setActionCommand("M");
//        radioFemale.setActionCommand("F");
//        radioOther.setActionCommand("O");
//
//
//        //SETUP combo box for counties
//        comboCounty = new JComboBox();
//        DefaultComboBoxModel comboCountyModel = new DefaultComboBoxModel();
//        for (String county: countiesIreland) {
//            comboCountyModel.addElement(county);
//        }
//
//        comboCounty.setModel(comboCountyModel);
//        //NOTE: if country is not ireland very useful for later
//        comboCounty.setSelectedIndex(countiesIreland.length-1);
//        comboCounty.setEnabled(false);
//        //SETUP combo box for age 1 - 110
//        comboAge = new JComboBox();
//        DefaultComboBoxModel comboAgeModel = new DefaultComboBoxModel();
//        for (int i = 1; i <= 110; i++) {
//            comboAgeModel.addElement(i);
//        }
//        comboAge.setModel(comboAgeModel);
//        comboAge.setPreferredSize(new Dimension(100,25));
//
//
//        btn = new JButton("press me");
//        //btn.setEnabled(false);
////        panelSignUp.add(radioMale);
////        panelSignUp.add(radioFemale);
////        panelSignUp.add(radioOther);
////        panelSignUp.add(comboCounty);
////        panelSignUp.add(comboAge);
////        panelSignUp.add(btn);
//        return panelSignUpCenter;
//    }
}
