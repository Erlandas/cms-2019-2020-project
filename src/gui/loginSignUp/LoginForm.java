package gui.loginSignUp;

/**
 * @author  Erlandas Bacauskas
 * Description:
 *      Simple Login Form
 *      That takes credentials and gives access if user is valid
 *      Also decides if admin or user section must be called
 */

import controller.DBControls;
import gui.admin.AdminMain;
import gui.customer.CustomerMain;
import gui.shared.SharedFunctions;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class LoginForm extends JFrame {

    private JPanel panel;
    private JLabel emailLabel;
    private JLabel passwordLabel;
    private JTextField emailField;
    private JPasswordField passwordField;
    private JButton btnLogin;
    private JButton btnRegister;

    /**
     * <br><b>Description</b> : <br>
     * Constructor that does initial setup of login window while using following private methods :
     *      1.  Establishes connection with database
     *      2.  Does initial setup of JFrame
     *      3.  Initialises components
     *      4.  Layout components
     *      5.  Adds component controls
     */

    public LoginForm() {
        connectDB();
        setupJFrame();
        initialisePanelComponents();
        layoutPanelComponents();
        buttonControls();
        add(panel);
    }

    private void setupJFrame() {
        //--    Set configuration for the frame
        setLocation(500, 150);
        setTitle("Login Form");
        setSize(270,180);
        setVisible(true);
        setResizable(false);

        //Default close operation also closes connection to DB
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing( WindowEvent e) {
                DBControls.closeConnection();
                e.getWindow().dispose();
            }
        });
    }

    private void buttonControls() {
        //--    Add Action listeners for the buttons
        //--    Using LAMBDAS
        SharedFunctions.buttonMouseEvents(btnLogin);
        SharedFunctions.buttonMouseEvents(btnRegister);
        btnLogin.addActionListener(al -> {

            emailLabel.setForeground(Color.black);
            passwordLabel.setForeground(Color.black);

            String emailAddress = emailField.getText();
            String password = passwordField.getText();

            if(emailAddress.isEmpty() && password.isEmpty()) {
                emailLabel.setForeground(Color.red);
                passwordLabel.setForeground(Color.red);
            } else if (emailAddress.isEmpty()) {
                emailLabel.setForeground(Color.red);
            } else if (password.isEmpty()) {
                passwordLabel.setForeground(Color.red);
            } else if (DBControls.validateUser(emailAddress,password)) {
                if("admin@admin.com".equals(emailAddress) && "admin123***".equals(password)) {
                    AdminMain ap = new AdminMain();
                    this.dispose();
                } else {
                    CustomerMain cp = new CustomerMain(emailAddress);
                    this.dispose();
                }

            } else {
                JOptionPane.showMessageDialog(
                        null,
                        "Password and Email Does Not Match",
                        "Warning",
                        JOptionPane.WARNING_MESSAGE);

            }
        });

        btnRegister.addActionListener(al -> {
            SignUpForm sf = new SignUpForm();
            this.dispose();
        });

        emailField.addActionListener(e -> btnLogin.doClick());
        passwordField.addActionListener(e -> btnLogin.doClick());
    }

    private void connectDB() {
        if (!DBControls.establishConnection()) {
            JOptionPane.showMessageDialog(
                    null,
                    "Could not connect to database\nPlease check your internet connection",
                    "Warning!",
                    JOptionPane.WARNING_MESSAGE);
            btnLogin.setEnabled(false);
            btnRegister.setEnabled(false);

        }
    }

    private void initialisePanelComponents() {
        //--    Initialising labels
        emailLabel = new JLabel("Email Address : ");
        passwordLabel = new JLabel("Password : ");

        //--   Initialising text/password fields
        passwordField = new JPasswordField(10);
        emailField = new JTextField(10);

        //--   Initialising button
        btnLogin = new JButton("Login");
        btnRegister = new JButton("Sign Up");

        //Setup buttons
        btnRegister.setBorder(new LineBorder(Color.WHITE));
        btnLogin.setBorder(new LineBorder(Color.WHITE));
        btnRegister.setPreferredSize(new Dimension(100,25));
        btnLogin.setPreferredSize(new Dimension(100,25));
        btnRegister.setBackground(Color.LIGHT_GRAY);
        btnLogin.setBackground(Color.LIGHT_GRAY);

    }

    private void layoutPanelComponents() {
        //-- Set-Up panel with the components
        panel = new JPanel(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();
        gc.fill = GridBagConstraints.NONE;

        int gridY = 0;
        SharedFunctions.gridLayoutAddComponent(panel,gc,emailLabel,0,gridY,1,1);
        SharedFunctions.gridLayoutAddComponent(panel,gc,emailField,1,gridY,1,1);

        gridY++;
        SharedFunctions.gridLayoutAddComponent(panel,gc,passwordLabel,0,gridY,1,2);
        SharedFunctions.gridLayoutAddComponent(panel,gc,passwordField,1,gridY,1,2);

        gridY++;
        SharedFunctions.gridLayoutAddComponent(panel,gc,btnRegister,0,gridY,1,1);
        SharedFunctions.gridLayoutAddComponent(panel,gc,btnLogin,1,gridY,1,1);

    }

}

