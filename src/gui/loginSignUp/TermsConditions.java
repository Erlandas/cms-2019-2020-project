package gui.loginSignUp;

/**
 * @author  Erlandas Bacauskas
 * Description:
 *      This class creates frame that displays terms and conditions
 */

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class TermsConditions extends JFrame {

    //Data needed for T&C frame
    private JTextArea textArea;
    private File file;
    private String st;
    private JScrollPane scroll;

    /**
     * <br><b>Description</b> : <br>
     * Constructor that creates terms and conditions frame with text appended
     * Calls private methods that does :
     *      1.  Setup JFrame
     *      2.  Appends text to JTextArea from a file
     */

    //Constructor that calls methods to setup T&C frame
    public TermsConditions() {
        setupTermsAndConditions();
        setupJFrame();
    }

    //--    Set configuration for the frame
    private void setupJFrame() {

        setLocation(350, 25);
        setTitle("Terms & Conditions");
        setVisible(true);
        setResizable(false);
        add(scroll);
        pack();
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                e.getWindow().dispose();
            }
        });
    }

    //Method that reads .TXT file and appends each line to the text area
    //--    Using bufferedReader, FileReader
    private void setupTermsAndConditions() {

        textArea = new JTextArea(35,50);
        file = new File("t&c.txt");
        try(BufferedReader br = new BufferedReader(new FileReader(file))) {

            while((st = br.readLine()) != null){
                textArea.append("    "+st+"\n");
            }

        } catch (IOException e) {
        }
        scroll = new JScrollPane(textArea);
    }
}
