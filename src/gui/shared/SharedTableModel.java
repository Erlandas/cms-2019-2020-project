package gui.shared;

/**
 * @author  Erlandas Bacauskas
 * <br><b>Description</b> : <br>
 *      This class creates TableModel for JTable
 *      While using result set passed as argument using mysql class
 */

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

public class SharedTableModel extends DefaultTableModel {

    //Parameters needed to create table model
    private ResultSet rs;
    private ResultSetMetaData metaData;
    private int numberOfColumns;
    private Vector<String> columnNames;
    private Vector<Vector<Object>> rows;
    private DefaultTableModel tableModel;

    //Constructor that accepts:
    //  ResultSet
    //  Vector with column names
    //Sets all data

    /**
     *
     * @param rs                -   type ResultSet
     * @param colNames          -   type Vector<String>
     * @throws SQLException     -   could throw SQLException
     *
     * <br><b>Description</b> : <br>
     *      If Vector == null gets default headings for columns from result set
     *      From passed result set gets data for :
     *      1.  MetaData
     *      2.  Column Count
     *      3.  Column names
     *      Initialises :
     *      1.  Vector of rows
     *      2.  Table model
     */
    public SharedTableModel (ResultSet rs, Vector<String> colNames) throws SQLException {
        this.rs = rs;
        this.metaData = rs.getMetaData();
        this.numberOfColumns = metaData.getColumnCount();
        this.columnNames = colNames==null? getColNames(): colNames;
        this.rows = new Vector<>();
        this.tableModel = new DefaultTableModel();
    }

    /**
     *
     * @param rs                -   type ResultSet
     * @throws SQLException     -   could throw SQLException
     *
     * <br><b>Description</b> : <br>
     *      Passes ResultSet to overloaded constructor and vector as null
     */
    //Constructor that accepts only result set and calls main constructor
    public SharedTableModel (ResultSet rs) throws SQLException {
        this(rs,null);
    }

    /**
     *
     * @return tableModel for JTable
     * @throws SQLException     -   could throw SQLException
     *
     * <br><b>Description</b> : <br>
     *      Generated table model from passed ResultSet
     */
    //Returns table model made up from the result set
    public TableModel getTableModel() throws SQLException {

        if(rs != null) {
            getDataForTableModel();
        }
        tableModel.setDataVector(this.rows,this.columnNames);
        return tableModel;
    }

    /**
     *
     * @return Vector<String> that holds values for column headings
     *         Extracted from metaData that was taken from ResultSet
     * @throws SQLException     -   could throw SQLException
     */
    //Method that sets all column names
    //--    using meta data from result set
    //--    if column names wasn't passed as parameter
    private Vector<String> getColNames() throws SQLException {

        Vector<String> colNames = new Vector<>();
        for (int column = 0; column < numberOfColumns; column++) {
            colNames.addElement(metaData.getColumnLabel(column + 1));
        }
        return colNames;
    }

    /**
     *
     * @throws SQLException     -   could throw SQLException
     *
     * <br><b>Description</b> : <br>
     *      Takes each row from result set ant populates Vector<Objects> with data
     *      then adds each row to Vector<Vector<Objects>>
     */
    //Sets all the rows from Result set that was passed in the constructor
    //--    While RS has next entry
    private void getDataForTableModel() throws SQLException {

        while (rs.next()) {
            Vector<Object> newRow = new Vector<>();
            for (int i = 1; i <= numberOfColumns; i++) {
                newRow.addElement(rs.getObject(i));
            }
            rows.addElement(newRow);
        }
    }
}
