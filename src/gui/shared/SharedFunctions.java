package gui.shared;

/**
 * @author Erlandas Bacauskas
 * Definition:
 *      This class holds shared methods between other classes
 *      Reduces duplication
 */

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class SharedFunctions {

    //Variables that belongs to buttonMouseEvents()
    private static LineBorder borderWhite = new LineBorder(Color.WHITE);
    private static LineBorder borderBlack = new LineBorder(Color.BLACK);

    /**
     * @param btn - accepts button as parameter JButton
     *            <br><b>Description</b> : <br>
     *              adds mouse listener to invert colours of  JButton
     *              mouse events used :
*                      1.  mouseEntered
*                          Sets background to white, sets black border, changes cursor to hand cursor
*                      2.  mouseExited
*                          Sets background to light grey, sets border to white, sets cursor to default arrow
     */

    //Adds mouse listener to buttons
    //To bring effect that seems the button its more or so alive and it does something
    public static void buttonMouseEvents(JButton btn) {

        btn.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                btn.setBackground(Color.WHITE);
                btn.setBorder(borderBlack);
                btn.setCursor(new Cursor(Cursor.HAND_CURSOR));

            }
            @Override
            public void mouseExited(MouseEvent e) {
                btn.setBackground(Color.LIGHT_GRAY);
                btn.setBorder(borderWhite);
                btn.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
        });
    }

    //Methods that helps with Grid Bag Layout
    //Accepts:
    //--    1.  Panel
    //--    2.  GridBagConstrains
    //--    3.  Component (textField, textArea, label, box, comboBox... etc...)
    //--    4.  Coordinates where to add (gridX and gridY)
    //--    5.  Line weight (weightX and weightY)

    /**
     *
     * @param panel     - type JPanel
     * @param gc        - type GridBagConstrains
     * @param label     - type JLabel
     * @param gridX     - type Integer
     * @param gridY     - type Integer
     * @param weightX   - type double
     * @param weightY   - type double
     *
     * <br><b>Description</b> : <br> positions JLable at the given coordinates on JPanel also as sets weight to JLabel
     */
    public static void gridLayoutAddComponent(
            JPanel panel, GridBagConstraints gc, JLabel label, int gridX, int gridY, double weightX, double weightY) {
        gc.weightx = weightX;
        gc.weighty = weightY;
        gc.gridx = gridX;
        gc.gridy = gridY;
        gc.insets = new Insets(5,10,0,10);
        gc.anchor = GridBagConstraints.FIRST_LINE_START;
        panel.add(label,gc);
    }

    /**
     *
     * @param panel     - type JPanel
     * @param gc        - type GridBagConstrains
     * @param textField - type JTextField
     * @param gridX     - type Integer
     * @param gridY     - type Integer
     * @param weightX   - type double
     * @param weightY   - type double
     *
     * <br><b>Description</b> : <br> positions JTextField at the given coordinates on JPanel also as sets weight to JLabel
     */
    public static void gridLayoutAddComponent(
            JPanel panel, GridBagConstraints gc, JTextField textField, int gridX, int gridY, double weightX, double weightY) {
        gc.weightx = weightX;
        gc.weighty = weightY;
        gc.gridx = gridX;
        gc.gridy = gridY;
        gc.insets = new Insets(5,10,0,10);
        gc.anchor = GridBagConstraints.FIRST_LINE_START;
        panel.add(textField,gc);
    }

    /**
     *
     * @param panel     - type JPanel
     * @param gc        - type GridBagConstrains
     * @param box       - type Box
     * @param gridX     - type Integer
     * @param gridY     - type Integer
     * @param weightX   - type double
     * @param weightY   - type double
     *
     * <br><b>Description</b> : <br> positions Box at the given coordinates on JPanel also as sets weight to JLabel
     */
    public static void gridLayoutAddComponent(
            JPanel panel, GridBagConstraints gc, Box box, int gridX, int gridY, double weightX, double weightY) {
        gc.weightx = weightX;
        gc.weighty = weightY;
        gc.gridx = gridX;
        gc.gridy = gridY;
        gc.insets = new Insets(5,10,0,10);
        gc.anchor = GridBagConstraints.FIRST_LINE_START;
        panel.add(box,gc);
    }

    /**
     *
     * @param panel     - type JPanel
     * @param gc        - type GridBagConstrains
     * @param combo     - type JComboBox
     * @param gridX     - type Integer
     * @param gridY     - type Integer
     * @param weightX   - type double
     * @param weightY   - type double
     *
     * <br><b>Description</b> : <br> positions JComboBox at the given coordinates on JPanel also as sets weight to JLabel
     */
    public static void gridLayoutAddComponent(
            JPanel panel, GridBagConstraints gc, JComboBox combo, int gridX, int gridY, double weightX, double weightY) {
        gc.weightx = weightX;
        gc.weighty = weightY;
        gc.gridx = gridX;
        gc.gridy = gridY;
        gc.insets = new Insets(5,10,0,10);
        gc.anchor = GridBagConstraints.FIRST_LINE_START;
        panel.add(combo,gc);
    }

    /**
     *
     * @param panel     - type JPanel
     * @param gc        - type GridBagConstrains
     * @param radio     - type JRadioButton
     * @param gridX     - type Integer
     * @param gridY     - type Integer
     * @param weightX   - type double
     * @param weightY   - type double
     *
     * <br><b>Description</b> : <br> positions JRadioButton at the given coordinates on JPanel also as sets weight to JLabel
     */
    public static void gridLayoutAddComponent(
            JPanel panel, GridBagConstraints gc, JRadioButton radio, int gridX, int gridY, double weightX, double weightY) {
        gc.weightx = weightX;
        gc.weighty = weightY;
        gc.gridx = gridX;
        gc.gridy = gridY;
        gc.insets = new Insets(5,10,0,10);
        gc.anchor = GridBagConstraints.FIRST_LINE_START;
        panel.add(radio,gc);
    }

    /**
     *
     * @param panel     - type JPanel
     * @param gc        - type GridBagConstrains
     * @param btn       - type JButton
     * @param gridX     - type Integer
     * @param gridY     - type Integer
     * @param weightX   - type double
     * @param weightY   - type double
     *
     * <br><b>Description</b> : <br> positions JRadioButton at the given coordinates on JPanel also as sets weight to JLabel
     */
    public static void gridLayoutAddComponent(
            JPanel panel, GridBagConstraints gc, JButton btn, int gridX, int gridY, double weightX, double weightY) {
        gc.weightx = weightX;
        gc.weighty = weightY;
        gc.gridx = gridX;
        gc.gridy = gridY;
        gc.insets = new Insets(5,10,0,10);
        gc.anchor = GridBagConstraints.FIRST_LINE_START;
        panel.add(btn,gc);
    }

    /**
     *
     * @param p     - type JPanel
     * @param s     - type JScrollPane
     *
     * <br><b>Description</b> : <br> removes existing components from JPanel and adds back JScrolPane
     *              then revalidate and repaint JPanel with new component added
     */
    //Repaints panel needed for JTable changes
    public static void refreshTable(JPanel p, JScrollPane s) {
        p.removeAll();
        p.add(s,BorderLayout.CENTER);
        p.revalidate();
        p.repaint();
    }
}
