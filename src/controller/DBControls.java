package controller;

/**
 * @author  Erlandas Bacauskas
 * <br><b>Description</b> : <br>
 *      This class CONTROLS connection between GUI elements and DATABASE
 *      Acts ass middle man between those components
 *      GUI calls Controller, Controller calls Database
 *      This way all JDBC and GUI components are separate
 *      No direct communication
 *
 */

import db.Customer;
import db.Datasource;
import db.Product;
import db.TableActiveInvoiceRecords;
import db.TableBasket;
import db.TableCustomer;
import db.TableInvoice;
import db.TableProduct;

import javax.swing.JTable;
import java.util.ArrayList;
import java.util.Vector;

public class DBControls {
   // private static Datasource DB = new Datasource();

    //todo CONTROLS MAIN DATABASE

    /**
     *
     * @return type boolean
     * <br><b>Description</b> : <br> establishes connection with database
     */
    public static boolean establishConnection() {
        return Datasource.establishConnection();
    }

    /**
     * Closes connection to database
     */
    public static void closeConnection(){
        Datasource.closeConnection();
    }

    //todo CONTROLS CUSTOMER TABLE

    /**
     *
     * @param email -   type string
     * @return boolean
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      checks if email is taken
     */
    public static boolean isEmailValid(String email) {
        return TableCustomer.isEmailPresent(email);
    }

    /**
     *
     * @param email -   type String
     * @param password  -   type String
     * @return boolean
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      checks if user is present in database table
     */
    public static boolean validateUser(String email, String password) {
        return TableCustomer.isUserPresent(email,password);
    }

    /**
     *
     * @param email         -   type String
     * @param firstName     -   type String
     * @param lastName      -   type String
     * @param addL1         -   type String
     * @param addL2         -   type String
     * @param town          -   type String
     * @param county        -   type String
     * @param country       -   type String
     * @param password      -   type String
     * @param phone         -   type String
     * @param gender        -   type String
     * @param age           -   type Integer
     * @return boolean
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      creates user type entry in the database table of users
     */
    public static boolean createCustomer(
            String email, String firstName, String lastName, String addL1, String addL2, String town,
            String county, String country, String password, String phone, String gender, int age) {
        return TableCustomer.createCustomer(
                email, firstName, lastName, addL1,addL2,  town,
                county, country, password, phone, gender, age);
    }

    /**
     *
     * @param id    -   type Integer
     * @return boolean
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      removes user from customers table using matching ID
     */
    public static boolean removeCustomer(int id) {
        return TableCustomer.removeCustomer(id);
    }

    /**
     *
     * @param colNames  -   type Vector<String>
     * @return JTable
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates JTable from database table
     */
    public static JTable getCustomerTable(Vector<String> colNames) {
        return TableCustomer.getCustomerTableReady(colNames);
    }

    /**
     *
     * @return JTable
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates JTable from database table
     */
    public static JTable getCustomerTable() {
        return TableCustomer.getCustomerTableReady();
    }

    /**
     *
     * @return ArrayList<Customer>
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Puts all Customer object data from customer table to list
     */
    public static ArrayList getCustomers() {
        return TableCustomer.getCustomers();
    }

    /**
     *
     * @param ID            -   type Integer
     * @param newPassword   -   type String
     * @return boolean
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Updates customer password in customer table entry
     */
    public static boolean updateCustomerPassword(int ID, String newPassword) {
        return TableCustomer.updateCustomerPassword(ID, newPassword);
    }

    /**
     *
     * @param email -   type String
     * @return Customer
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Retrieves currently active customer
     */
    public static Customer getActiveCustomer(String email) {
        return TableCustomer.getActiveCustomer(email);
    }

    //todo CONTROLS PRODUCT TABLE

    /**
     *
     * @param name          -   type String
     * @param brand         -   type String
     * @param description   -   type String
     * @param price         -   type String
     * @param quantity      -   type String
     * @param location      -   type String
     * @return boolean
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates product type entry in product table in the database
     */
    public static boolean createProduct(
            String name, String brand, String description, String price, String quantity, String location) {
        return TableProduct.createProduct(name, brand, description, price, quantity, location);
    }

    /**
     *
     * @return ArrayList<Product>
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Puts all Product object data from product table to list
     */
    public static ArrayList getProducts() {
        return TableProduct.getProducts();
    }

    /**
     *
     * @param id    -   type Integer
     * @return boolean
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      removes product from product table using matching ID
     */
    public static boolean removeProduct(int id) {
        return TableProduct.removeProduct(id);
    }

    /**
     *
     * @param name          -   type String
     * @param brand         -   type String
     * @param description   -   type String
     * @param price         -   type String
     * @param quantity      -   type String
     * @param location      -   type String
     * @param iD            -   type Integer
     * @return boolean
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      updates existing product record in product table
     */
    public static boolean updateProduct(String name, String brand, String description, String price, String quantity, String location, int iD) {
        return TableProduct.updateProduct(name, brand, description, price, quantity, location, iD);
    }

    /**
     *
     * @param colNames  -   type Vector<String>
     * @return JTable
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates JTable from database table
     */
    public static JTable getProductTable(Vector<String> colNames) {
        return TableProduct.getProductTableReady(colNames);
    }

    /**
     *
     * @return JTable
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates JTable from database table
     */
    public static JTable getProductTable() {
        return TableProduct.getProductTableReady();
    }

    //todo CONTROLS BASKET TABLE

    /**
     *
     * @param tableName -   type String
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates SQL syntax for basket table
     */
    public static void setSqlBasketCreateSqlSyntax(String tableName) {
        TableBasket.setSqlBasketCreateSqlSyntax(tableName);
    }

    /**
     *
     * @return boolean
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates basket table in database
     */
    public static boolean basketCreate() {
        return TableBasket.basketCreate();
    }

    /**
     *
     * @param p         -   type Product
     * @param quantity  -   type Integer
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Adds product to basket table
     */
    public static void basketAddTo(Product p, String quantity) {
        TableBasket.basketAddTo(p, quantity);
    }

    /**
     *
     * @param id    -   type Integer
     * @return boolean
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      removes product from basket table using matching ID
     */
    public static boolean basketRemoveItem(int id) {
        return TableBasket.basketRemoveItem(id);
    }

    /**
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      deletes basket table in database
     */
    public static void basketDeleteTable() {
        TableBasket.basketDeleteTable();
    }

    /**
     *
     * @param colNames  -   type Vector<String>
     * @return JTable
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates JTable from database table
     */
    public static JTable getBasketTableReady(Vector<String> colNames) {
        return TableBasket.getBasketTableReady(colNames);
    }

    /**
     *
     * @return JTable
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates JTable from database table
     */
    public static JTable getBasketTableReady() {
        return getBasketTableReady(null);
    }

    //todo CONTROLS ACTIVE INVOICE

    /**
     *
     * @param invoiceName   -   type String
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Adds invoice record to active invoice records table
     */
    public static void invoiceAddRecord(String invoiceName) {
        TableActiveInvoiceRecords.invoiceAddRecord(invoiceName);
    }

    /**
     *
     * @param colNames  -   type Vector<String>
     * @return JTable
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates JTable from database table
     */
    public static JTable getInvoicesTableReady(Vector<String> colNames) {
        return TableActiveInvoiceRecords.getActiveInvoiceTableReady(colNames);
    }

    /**
     *
     * @return JTable
     * D<br><b>Description</b> : <br>
     *      Contacts database
     *      Creates JTable from database table
     */
    public static JTable getInvoicesTableReady() {
        return getInvoicesTableReady(null);
    }

    //todo CONTROLS INVOICE TABLE

    /**
     *
     * @param tableName -   type String
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates SQL syntax for invoice table
     */
    public static void setInvoiceSqlSyntax(String tableName) {
        TableInvoice.setInvoiceSqlSyntax(tableName);
    }

    /**
     *
     * @param colNames  -   type Vector<String>
     * @return JTable
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates JTable from database table
     */
    public static JTable getInvoiceProducts(Vector<String> colNames) {
        return TableInvoice.getInvoiceProducts(colNames);
    }

    /**
     *
     * @return JTable
     * <br><b>Description</b> : <br>
     *      Contacts database
     *      Creates JTable from database table
     */
    public static JTable getInvoiceProducts() {
        return getInvoiceProducts(null);
    }



























}
